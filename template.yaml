Transform: AWS::Serverless-2016-10-31

Parameters:
  EnvironmentStackName:
    Type: String
    AllowedValues: [qa, prod]

Mappings:
  Config:
    qa:
      SigningKey: "{{resolve:secretsmanager:qa/signingKey:SecretString:secret}}"
      MinCapacity: 0
      MaxCapacity: 1
      ProvConcurrentExecutions: 1
      PreWarmDynamoClient: true
      ReservedConcurrent: 1
    prod:
      SigningKey: "{{resolve:secretsmanager:prod/signingKey:SecretString:secret}}"
      MinCapacity: 1
      MaxCapacity: 5
      ProvConcurrentExecutions: 10
      PreWarmDynamoClient: true
      ReservedConcurrent: 5

Globals:
  Function:
    Runtime: "java8.al2"
    Timeout: 20
    Tracing: Active
    MemorySize: 448
    AutoPublishAlias: live
    ReservedConcurrentExecutions: !FindInMap [ Config, !Ref EnvironmentStackName, ReservedConcurrent ]
    Tags:
      "user:cost-center": "000000000000-common-settings"
    DeploymentPreference:
      Type: AllAtOnce
    Environment:
      Variables:
        signing_key: !FindInMap [ Config, !Ref EnvironmentStackName, SigningKey ]
        profile: LIVE
        table_name: !Ref CommonSettingsTable
        warm_dynamo_client: !FindInMap [ Config, !Ref EnvironmentStackName, PreWarmDynamoClient ]

    VpcConfig:
      SecurityGroupIds:
        - Fn::ImportValue:
            !Sub "${EnvironmentStackName}-NATSecurityGroup"
      SubnetIds:
        Fn::Split:
          - ","
          - Fn::ImportValue:
              !Sub "${EnvironmentStackName}-BackendVpcSubnets"

Resources:
  CommonSettingsApiFunction:
    Type: AWS::Serverless::Function
    Properties:
      CodeUri: "api"
      Handler: "mycompany.rs.api.CommonSettingsApiFunction"

      ProvisionedConcurrencyConfig:
        ProvisionedConcurrentExecutions: 1
      ReservedConcurrentExecutions: !FindInMap [ Config, !Ref EnvironmentStackName, ReservedConcurrent ]

      Events:
        ApiEvent:
          Type: Api
          Properties:
            Method: any
            Path: /{proxy+}
            RestApiId:
              Ref: ApiGatewayApi

      Policies:
        - DynamoDBCrudPolicy:
            TableName: !Ref CommonSettingsTable

  ApiGatewayApi:
    Type: AWS::Serverless::Api
    Properties:
      StageName: !Ref EnvironmentStackName

  CommonSettingsTable:
    Type: AWS::DynamoDB::Table
    Properties:
      BillingMode: PAY_PER_REQUEST
      AttributeDefinitions:
        - AttributeName: identifier
          AttributeType: S
        - AttributeName: namespace
          AttributeType: S
      KeySchema:
        - AttributeName: identifier
          KeyType: HASH
        - AttributeName: namespace
          KeyType: RANGE

  CommonSettingsApiFunctionScalableTarget:
    Type: AWS::ApplicationAutoScaling::ScalableTarget
    Properties:
      MaxCapacity: !FindInMap [ Config, !Ref EnvironmentStackName, MaxCapacity ]
      MinCapacity: !FindInMap [ Config, !Ref EnvironmentStackName, MinCapacity ]
      ResourceId: !Sub function:${CommonSettingsApiFunction}:live
      RoleARN: !Sub arn:aws:iam::${AWS::AccountId}:role/aws-service-role/lambda.application-autoscaling.amazonaws.com/AWSServiceRoleForApplicationAutoScaling_LambdaConcurrency
      ScalableDimension: lambda:function:ProvisionedConcurrency
      ServiceNamespace: lambda
    DependsOn: CommonSettingsApiFunctionAliaslive

  CommonSettingsAPiFunctionScalingPolicy:
    Type: AWS::ApplicationAutoScaling::ScalingPolicy
    Properties:
      PolicyName: utilization
      PolicyType: TargetTrackingScaling
      ScalingTargetId: !Ref CommonSettingsApiFunctionScalableTarget
      TargetTrackingScalingPolicyConfiguration:
        TargetValue: 0.4
        PredefinedMetricSpecification:
          PredefinedMetricType: LambdaProvisionedConcurrencyUtilization

  CommonSettingsApiService:
    Type: AWS::ServiceDiscovery::Service
    Properties:
      Name: CommonSettingsApiFunction
      NamespaceId:
        Fn::ImportValue:
          !Sub "${EnvironmentStackName}-ServiceDiscoveryNamespaceId"

  RatingsServiceInstance:
    Type: AWS::ServiceDiscovery::Instance
    Properties:
      InstanceAttributes:
        AWS_INSTANCE_CNAME: !Sub "https://${ApiGatewayApi}.execute-api.${AWS::Region}.amazonaws.com/${EnvironmentStackName}/"
      InstanceId: !Ref ApiGatewayApi
      ServiceId: !Ref CommonSettingsApiService
