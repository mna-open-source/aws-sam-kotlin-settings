plugins {
    val kotlinVersion = "1.4.10"
    kotlin("jvm") version kotlinVersion apply false
}

subprojects {
    repositories {
        mavenCentral()
        mavenLocal()
    }
}
