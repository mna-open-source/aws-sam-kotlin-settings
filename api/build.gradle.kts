plugins {
    kotlin("jvm")
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:1.4.0")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.12.5") {
        exclude("org.jetbrains.kotlin:kotlin-reflect")
    }
    implementation("org.jetbrains.kotlin:kotlin-reflect:1.4.0")

    //jackson
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.12.5")
    implementation("com.fasterxml.jackson.datatype:jackson-datatype-jsr310:2.12.5")
    implementation("com.fasterxml.jackson.dataformat:jackson-dataformat-ion:2.12.5")
    implementation("com.fasterxml.jackson.dataformat:jackson-dataformat-cbor:2.12.5")

    //aws
    implementation(platform("software.amazon.awssdk:bom:2.14.23"))
    implementation("com.amazonaws:aws-lambda-java-core:1.2.1")
    implementation("com.amazonaws:aws-lambda-java-events:3.10.0")
    implementation("software.amazon.awssdk:dynamodb:2.17.34") {
        exclude("software.amazon.awssdk", "apache-client")
        exclude("software.amazon.awssdk", "netty-nio-client")
    }
    implementation("software.amazon.awssdk:url-connection-client:2.17.34")

    //security
    implementation("com.auth0:java-jwt:3.18.1")

    //logging
    implementation("org.apache.logging.log4j:log4j-api:2.14.1")
    implementation("org.apache.logging.log4j:log4j-core:2.14.1")
    runtimeOnly("org.apache.logging.log4j:log4j-slf4j18-impl:2.14.1")
    runtimeOnly("com.amazonaws:aws-lambda-java-log4j2:1.2.0")

    testImplementation("org.jetbrains.kotlin:kotlin-test-junit:1.5.21")
    testImplementation("com.nhaarman.mockitokotlin2:mockito-kotlin:2.2.0")
    testImplementation("junit:junit:4.13.2")
    testImplementation("org.apache.logging.log4j:log4j-slf4j18-impl:2.14.1")
    testImplementation("org.mockito:mockito-core:3.12.4")
    testImplementation("org.mockito:mockito-inline:3.12.4")
    testImplementation("org.hamcrest:hamcrest:2.2")
    testImplementation("org.testcontainers:localstack:1.16.0")

    testRuntimeOnly("com.amazonaws:aws-java-sdk-dynamodb:1.12.62")
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

tasks.test {
    reports.html.destination = file("$buildDir/html")
}
