package mycompany.rs

fun <F, S> F.appendToTuple(block: (F) -> S): Pair<F, S> = Pair(this, block(this))

fun <F, S> Pair<F, S>.tupleAlso(block: (F, S) -> Unit): Pair<F, S> = this.also { block(first, second) }

fun <F, S, R> Triple<F, S, R>.tripleAlso(block: (F, S, R) -> Unit): Triple<F, S, R> = this.also { block(first, second, third) }

fun <F, S, R> Pair<F, S>.tupleLet(block: (F, S) -> R): R = block(first, second)

fun <F, S, R> Pair<F, S>.tupleLetFirst(block: (F, S) -> R): Pair<R, S> = Pair(block(first, second), second)

fun <F, S, R> Pair<F, S>.tupleLetSecond(block: (F, S) -> R): Pair<F, R> = Pair(first, block(first, second))
