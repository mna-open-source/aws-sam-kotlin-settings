package mycompany.rs.api

import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.RequestHandler
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent
import com.fasterxml.jackson.databind.ObjectMapper
import org.slf4j.LoggerFactory
import mycompany.rs.Configuration.apiRouter
import mycompany.rs.Configuration.exceptionHandler
import mycompany.rs.Configuration.mapper
import mycompany.rs.PagedCommonSettings
import mycompany.rs.CommonSetting
import mycompany.rs.SearchObject
import java.lang.RuntimeException

class CommonSettingsApiFunction(
    private val apiRouter: ApiRouter,
    private val exceptionHandler: ExceptionHandler,
    private val mapper: ObjectMapper
) : RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {

    @Suppress("unused")
    constructor() : this(
        apiRouter = apiRouter(),
        exceptionHandler = exceptionHandler(),
        mapper = mapper()
    )

    companion object {
        private val log = LoggerFactory.getLogger(CommonSettingsApiFunction::class.java)
        const val basePath = "/common-setting"
        const val findByIdentifier = "/common-settings-by-identifier"
        private const val authorizationHeader = "Authorization"
    }

    override fun handleRequest(event: APIGatewayProxyRequestEvent, context: Context): APIGatewayProxyResponseEvent =
        try {
            event
                .also { log.debug(event.toString(authorizationHeader)) }
                .let { handleEvent(event) }
        } catch (t: Throwable) {
            exceptionHandler.handle(t)
        }

    private fun handleEvent(event: APIGatewayProxyRequestEvent): APIGatewayProxyResponseEvent =
        when (event.path) {
            basePath -> handleBasePath(event)
            findByIdentifier -> handleFindByIdentifierPath(event)
            else -> notFound(event.path)
        }

    private fun handleBasePath(event: APIGatewayProxyRequestEvent): APIGatewayProxyResponseEvent =
        when (event.httpMethod) {
            "GET" -> findCommonSetting(event)
            "POST" -> saveCommonSetting(event, authorization(event))
            else -> methodNotAllowed()
        }

    private fun handleFindByIdentifierPath(event: APIGatewayProxyRequestEvent) =
        when (event.httpMethod) {
            "GET" -> findCommonSettings(event)
            else -> methodNotAllowed()
        }

    private fun findCommonSetting(event: APIGatewayProxyRequestEvent): APIGatewayProxyResponseEvent =
        findSetting(event.toSearchObject())
            ?.asResponse()
            ?: notFound("${event.path} - ${event.queryStringParameters}")

    private fun findCommonSettings(event: APIGatewayProxyRequestEvent): APIGatewayProxyResponseEvent =
        findSettings(event.toFindByIdentifierParams()).asResponse()

    private fun findSetting(searchObject: SearchObject) = apiRouter.findSetting(searchObject)
    private fun findSettings(params: Pair<String, String?>) = apiRouter.findByIdentifier(params.first, params.second)

    private fun saveCommonSetting(event: APIGatewayProxyRequestEvent, authorization: String) =
        event.body.takeIf { it != null }?.let {
            apiRouter.save(it.asCommonSetting(), authorization)
        }?.asResponse() ?: badRequest(errorResponse())

    private fun authorization(event: APIGatewayProxyRequestEvent) =
        event.headers?.getValue(authorizationHeader)?.removePrefix("Bearer ") ?: throw MissingAuthorizationHeader()

    private fun CommonSetting.asResponse() = ok(
        mapper.writeValueAsString(this)
    )

    private fun PagedCommonSettings.asResponse() = ok(
        mapper.writeValueAsString(this)
    )

    private fun errorResponse() = ErrorResponse(
        setOf(
            ErrorWrapper(
                entity = "request",
                property = "body",
                invalidValue = "",
                message = "missing body"
            )
        )
    ).let {
        mapper.writeValueAsString(it)
    }
}

class NotFound(reason: String) : RuntimeException(reason)
class MissingAuthorizationHeader : RuntimeException()
