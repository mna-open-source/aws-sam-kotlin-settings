package mycompany.rs.api

import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.MissingKotlinParameterException
import org.slf4j.LoggerFactory
import mycompany.rs.Forbidden

interface ExceptionHandler {
    fun handle(e: Throwable): APIGatewayProxyResponseEvent
}

class DefaultExceptionHandler(
    private val mapper: ObjectMapper
) : ExceptionHandler {

    companion object {
        private val log = LoggerFactory.getLogger(DefaultExceptionHandler::class.java)
    }

    override fun handle(e: Throwable): APIGatewayProxyResponseEvent {
        log.warn("about to handle exception", e)
        return when (e) {
            is MissingAuthorizationHeader -> accessDenied()
            is InvalidToken -> accessDenied()
            is Forbidden -> forbidden(e.message!!)
            is MissingKotlinParameterException -> badRequest(e.asErrorResponse())
            is MissingQueryString -> badRequest(e.asErrorResponse())
            is MissingQueryParams -> badRequest(e.asErrorResponse())
            is NotFound -> notFound(e.message!!)
            else -> internalServerError(e.message)
        }
    }

    private fun MissingQueryParams.asErrorResponse() = ErrorResponse(
        setOf(
            ErrorWrapper(
                entity = "request",
                property = "query parameters",
                invalidValue = "",
                message = "${this.message}"
            )
        )
    ).asJsonString()

    private fun MissingKotlinParameterException.asErrorResponse() =
        ErrorResponse(
            setOf(
                ErrorWrapper(
                    entity = "request",
                    property = "body",
                    invalidValue = "",
                    message = "missing parameter: ${this.parameter.name}"
                )
            )
        ).asJsonString()

    private fun MissingQueryString.asErrorResponse() =
        ErrorResponse(
            setOf(
                ErrorWrapper(
                    entity = "request",
                    property = this.parameter,
                    invalidValue = "",
                    message = this.message!!
                )
            )
        ).asJsonString()

    private fun ErrorResponse.asJsonString(): String =
        mapper.writeValueAsString(this)

}

data class ErrorResponse(
    val errors: Set<ErrorWrapper>
)

data class ErrorWrapper(
    val entity: String,
    val property: String,
    val invalidValue: String,
    val message: String
)

class MissingQueryParams : RuntimeException("missing query parameters")
class InvalidToken(parameter: String) : RuntimeException("invalid token: $parameter")
data class MissingQueryString(val parameter: String) : RuntimeException("missing parameter: $parameter")
