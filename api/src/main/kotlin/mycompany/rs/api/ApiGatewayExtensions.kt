package mycompany.rs.api

import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent
import com.fasterxml.jackson.databind.JsonNode
import mycompany.rs.Configuration.mapper
import mycompany.rs.CommonSetting
import mycompany.rs.SearchObject

fun APIGatewayProxyRequestEvent.toSearchObject(): SearchObject = this.queryStringParameters?.let {
    SearchObject(
        identifier = this.requiredParam("identifier"),
        namespace = this.requiredParam("namespace")
    )
} ?: throw MissingQueryParams()

fun APIGatewayProxyRequestEvent.toFindByIdentifierParams() = this.queryStringParameters?.let {
    this.requiredParam("identifier") to this.optionalParam("scroll")
} ?: throw MissingQueryParams()

fun APIGatewayProxyRequestEvent.optionalParam(param: String) = this.queryStringParameters?.get(param)

fun APIGatewayProxyRequestEvent.requiredParam(param: String) = optionalParam(param) ?: throw MissingQueryString(param)

fun APIGatewayProxyRequestEvent.toString(headerToExclude: String): String {
    val nonSensibleHeaders = this.headers?.filter { it.key != headerToExclude }
    val nonSensibleMultiValueHeaders = this.multiValueHeaders?.filter { it.key != headerToExclude }
    return listOf(
        "resource" to resource,
        "path" to path,
        "httpMethod" to httpMethod,
        "headers" to nonSensibleHeaders,
        "multiValueHeaders" to nonSensibleMultiValueHeaders,
        "queryStringParameters" to queryStringParameters,
        "multiValueQueryStringParameters" to multiValueQueryStringParameters,
        "pathParameters" to pathParameters,
        "stageVariables" to stageVariables,
        "requestContext" to requestContext,
        "body" to body,
        "isBase64Encoded" to isBase64Encoded
    )
        .filter { it.second != null }
        .joinToString(",") { "${it.first}: ${it.second}" }
}

fun String.asCommonSetting() =
    mapper().readTree(this).let { node ->
        CommonSetting(
            identifier = node.asRequiredText("identifier"),
            namespace = node.asRequiredText("namespace"),
            value = node.asRequiredText("value")
        )
    }

fun accessDenied() =
    APIGatewayProxyResponseEvent()
        .withStatusCode(401)!!

fun badRequest(body: String) =
    APIGatewayProxyResponseEvent()
        .withStatusCode(400)
        .withBody(body)!!

fun forbidden(body: String) =
    APIGatewayProxyResponseEvent()
        .withStatusCode(403)
        .withBody(body)!!

fun internalServerError(message: String?) =
    APIGatewayProxyResponseEvent()
        .withStatusCode(500)
        .withBody(message)!!

fun methodNotAllowed() =
    APIGatewayProxyResponseEvent()
        .withStatusCode(405)!!

fun notFound(path: String) =
    APIGatewayProxyResponseEvent()
        .withStatusCode(404)
        .withBody(path)!!

fun ok(body: String) =
    APIGatewayProxyResponseEvent()
        .withStatusCode(200)
        .withHeaders(mapOf("Content-Type" to "application/json"))
        .withBody(body)!!

private fun JsonNode.asRequiredText(field: String) = this.asRequired(field) { r -> r }

private fun <E> JsonNode.asRequired(field: String, transform: (String) -> E): E = this[field]?.asText()?.let(transform)
    ?: throw IllegalArgumentException("$field is required")

