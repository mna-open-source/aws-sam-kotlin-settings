package mycompany.rs.api

import org.slf4j.LoggerFactory
import mycompany.rs.*

interface ApiRouter {
    fun findByIdentifier(identifier: String, scroll: String?): PagedCommonSettings
    fun save(commonSetting: CommonSetting, authorization: String): CommonSetting
    fun findSetting(searchObject: SearchObject): CommonSetting?
}

class DefaultApiRouter(
    private val commonSettings: CommonSettings,
    private val security: Security
) : ApiRouter {

    companion object {
        private val log = LoggerFactory.getLogger(DefaultApiRouter::class.java)
    }

    override fun findByIdentifier(identifier: String, scroll: String?): PagedCommonSettings {
        log.debug("about to find common settings by identifier {} and scroll {}", identifier, scroll)
        return commonSettings.findByIdentifier(identifier, scroll)
    }

    override fun save(commonSetting: CommonSetting, authorization: String): CommonSetting {
        security.checkAllowedToSave(authorization)
        log.trace("about to save {}", commonSetting)
        return commonSettings.save(commonSetting)
    }

    override fun findSetting(searchObject: SearchObject): CommonSetting? =
        log.trace("about to find common setting for {}", searchObject).let {
            commonSettings.findBy(searchObject)
        }
}
