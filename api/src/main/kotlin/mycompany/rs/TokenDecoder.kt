package mycompany.rs

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import com.auth0.jwt.interfaces.DecodedJWT
import com.auth0.jwt.interfaces.JWTVerifier
import java.util.*

class TokenDecoder(secret: String) {

    private val verifier: JWTVerifier

    init {
        val algorithm = Algorithm.HMAC256(secret)

        verifier = JWT.require(algorithm)
            .withIssuer(DefaultSecurity.issuer)
            .build() //Reusable verifier instance
    }

    fun decode(token: String): DecodedJWT = verifier.verify(token)
}

fun DecodedJWT.authorities(): Array<String> = this.getClaim("authorities").asArray(String::class.java)

@Throws(Exception::class)
fun generateCredentials(
    secret: String,
    authorities: List<String>
): String {
    var builder = JWT.create()
        .withIssuedAt(Date())
        .withIssuer(DefaultSecurity.issuer)

    if (authorities.isNotEmpty()) {
        builder = builder.withArrayClaim("authorities", authorities.toTypedArray())
    }

    return builder.sign(Algorithm.HMAC256(secret))
}
