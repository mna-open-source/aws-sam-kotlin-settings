package mycompany.rs

import software.amazon.awssdk.services.dynamodb.DynamoDbClient
import java.time.Clock
import java.time.Instant

interface CommonSettings {
    fun findBy(searchObject: SearchObject): CommonSetting?
    fun findByIdentifier(identifier: String, scroll: String?): PagedCommonSettings
    fun queryBy(searchObject: SearchObject): List<CommonSetting>
    fun save(commonSetting: CommonSetting): CommonSetting
}

class DynamoCommonSettings(
    private val clock: Clock,
    private val dynamo: DynamoDbClient,
    private val table: String
) : CommonSettings {

    companion object {
        private const val idv = ":idv"
        private const val kv = ":kv"
    }

    override fun findBy(searchObject: SearchObject): CommonSetting? =
        dynamo.getItem {
            it
                .tableName(table)
                .key(searchObject.asKeyMap())
        }.let { response ->
            response.takeIf { it.hasItem() }?.item()?.asCommonSetting()
        }

    override fun findByIdentifier(identifier: String, scroll: String?): PagedCommonSettings =
        dynamo.query {
            it
                .tableName(table)
                .keyConditionExpression("${DynamoDBField.IDENTIFIER.lowercase()} = $idv")
                .expressionAttributeValues(
                    mapOf(
                        *identifier.asAttributeFor(idv)
                    )
                )
                .apply {
                    if (scroll != null) {
                        exclusiveStartKey(
                            mapOf(
                                *identifier.asAttribute(DynamoDBField.IDENTIFIER),
                                *scroll.asAttribute(DynamoDBField.NAMESPACE)
                            )
                        )
                    }
                }
        }.let { response ->
            response.takeIf { it.hasItems() }?.let {
                PagedCommonSettings(
                    identifier = identifier,
                    settings = it.items().asCommonSettings().sortedBy { e -> e.namespace },
                    scroll = it.lastEvaluatedKey().asScroll()
                )
            } ?: PagedCommonSettings(identifier)
        }

    override fun queryBy(searchObject: SearchObject): List<CommonSetting> =
        dynamo.query {
            it
                .tableName(table)
                .keyConditionExpression("${DynamoDBField.IDENTIFIER.lowercase()} = $idv and begins_with(${DynamoDBField.NAMESPACE.lowercase()}, $kv)")
                .expressionAttributeValues(
                    mapOf(
                        *searchObject.identifier.asAttributeFor(idv),
                        *searchObject.namespace.asAttributeFor(kv)
                    )
                )
        }.let { response ->
            response.takeIf { it.hasItems() }?.items()?.asCommonSettings() ?: emptyList()
        }

    override fun save(commonSetting: CommonSetting): CommonSetting =
        commonSetting.copy(lastUpdated = clock.instant()).let { request ->
            dynamo.putItem {
                it
                    .tableName(table)
                    .item(request.asItemMap())
            }
            request
        }
}

data class CommonSetting(
    val identifier: String,
    val namespace: String,
    val value: String,
    val lastUpdated: Instant? = null
)

data class PagedCommonSettings(
    val identifier: String,
    val settings: List<CommonSetting> = emptyList(),
    val scroll: String? = null
)

data class SearchObject(
    val identifier: String,
    val namespace: String
)

enum class DynamoDBField {
    IDENTIFIER, NAMESPACE, VALUE, LAST_UPDATED;

    fun lowercase() = this.name.toLowerCase()
}
