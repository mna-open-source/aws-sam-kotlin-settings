package mycompany.rs

import org.slf4j.LoggerFactory

interface Security {
    fun checkAllowedToSave(accessToken: String)
}

class DefaultSecurity(
    private val tokenDecoder: TokenDecoder
) : Security {

    companion object {
        const val issuer = "mycompany"
        private val allowedAuthorities = setOf("STORES_SAVE_PRICE_LIST")
        private val log = LoggerFactory.getLogger(DefaultSecurity::class.java)
    }

    override fun checkAllowedToSave(accessToken: String) {
        if (!accessToken.containsAllowedAuthorities()) {
            log.trace("{} is not allowed to save", accessToken.obfuscate())
            throw Forbidden("is not allowed to save settings")
        }
    }

    private fun String.containsAllowedAuthorities() =
        tokenDecoder.decode(this).authorities()
            .also {
                log.trace("found {} authorities for {}", it.size, this.obfuscate())
            }.any(allowedAuthorities::contains).also {
                log.trace("in authorities list, found allowed authorities '{}'", it)
            }
}

class Forbidden(message: String) : RuntimeException(message)
