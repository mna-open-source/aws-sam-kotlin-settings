package mycompany.rs

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.slf4j.LoggerFactory
import software.amazon.awssdk.auth.credentials.AwsCredentials
import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider
import software.amazon.awssdk.auth.credentials.EnvironmentVariableCredentialsProvider
import software.amazon.awssdk.core.client.config.ClientOverrideConfiguration
import software.amazon.awssdk.http.urlconnection.UrlConnectionHttpClient
import software.amazon.awssdk.regions.Region
import software.amazon.awssdk.services.dynamodb.DynamoDbClient
import mycompany.rs.api.*
import java.lang.UnsupportedOperationException
import java.net.URI
import java.time.Clock
import kotlin.reflect.KMutableProperty0

object Configuration {
    private val log = LoggerFactory.getLogger(Configuration::class.java)

    private lateinit var cachedApiRouter: ApiRouter
    private lateinit var cachedMapper: ObjectMapper

    private const val SIGNING_KEY = "signing_key"
    private const val PROFILE = "profile"
    private const val TABLE = "table_name"
    private const val WARM_DYNAMO_CLIENT = "warm_dynamo_client"

    fun apiRouter() = initProperty(::cachedApiRouter, ::cachedApiRouter.isInitialized) {
        DefaultApiRouter(
            commonSettings = commonSettings(),
            security = security()
        )
    }

    fun exceptionHandler() = DefaultExceptionHandler(mapper())

    fun mapper() = initProperty(::cachedMapper, ::cachedMapper.isInitialized) {
        jacksonObjectMapper()
    }

    private fun clock() = Clock.systemUTC()

    private fun commonSettings() = DynamoCommonSettings(
        clock = clock(),
        dynamo = DynamoDbClientBuilder(getSetting(PROFILE)).build(),
        table = getSetting(TABLE)
    ).also {
        if (getSetting(WARM_DYNAMO_CLIENT).toBoolean()) {
            log.info("calling dummy call for {} with result {}", it.javaClass, it.findBy(SearchObject("-", "-")))
        }
    }

    private fun security() = DefaultSecurity(
        tokenDecoder = getSetting(SIGNING_KEY).let {
            log.info("Initializing security with signing key {}", it.obfuscate())
            TokenDecoder(it)
        }
    )

    private fun <T> initProperty(property: KMutableProperty0<T>, isInitialized: Boolean, block: () -> T): T =
        when (isInitialized) {
            true -> property.get()
            else -> block().also {
                property.set(it)
                log.info("Init {} with {}", property.javaClass.simpleName, block.toString())
            }
        }

    private fun getSetting(key: String): String = System.getProperty(key, System.getenv(key)).apply {
        if (this.isBlank()) throw IllegalArgumentException("setting $key is required")
    }
}

sealed class ConfigurationBuilder<T>(
    protected val profile: String
) {
    abstract fun build(): T
}

class DynamoDbClientBuilder(
    providedProfile: String
) : ConfigurationBuilder<DynamoDbClient>(profile = providedProfile) {
    companion object {
        private const val local = "LOCAL"
        private const val live = "LIVE"
        private const val dynamoHost = "DYNAMO_HOST"
        private val defaultAwsRegion = Region.US_WEST_2
        private val log = LoggerFactory.getLogger(DynamoDbClientBuilder::class.java)
    }

    override fun build(): DynamoDbClient =
        when (profile) {
            live -> buildLiveClient()
            local -> buildLocalClient()
            else -> throw UnsupportedOperationException("profile $profile is not supported")
        }.also {
            log.info("Init DynamoDbAsyncClient as ${it.javaClass.simpleName} on profile $profile")
        }

    private fun buildLiveClient() =
        clientBuilder()
            .endpointOverride(URI.create("https://dynamodb.us-east-1.amazonaws.com"))
            .overrideConfiguration(ClientOverrideConfiguration.builder().build())
            .credentialsProvider(credentials())
            .build()

    private fun buildLocalClient() =
        clientBuilder()
            .credentialsProvider(object : AwsCredentialsProvider {
                override fun resolveCredentials(): AwsCredentials {
                    return object : AwsCredentials {
                        override fun accessKeyId(): String {
                            return "foo"
                        }

                        override fun secretAccessKey(): String {
                            return "foo"
                        }
                    }
                }
            }).region(defaultAwsRegion)
            .endpointOverride(URI.create(getDynamoHost())).build()

    private fun clientBuilder() =
        DynamoDbClient.builder()
            .region(region())
            .httpClient(http())

    private fun credentials() = EnvironmentVariableCredentialsProvider.create()

    private fun getDynamoHost(): String = System.getProperty(dynamoHost, System.getenv(dynamoHost))
        .takeIf { it != null && it.isNotBlank() } ?: "http://host.docker.internal:8000/"

    private fun http() = UrlConnectionHttpClient.builder().build()

    private fun region() = defaultAwsRegion
}

