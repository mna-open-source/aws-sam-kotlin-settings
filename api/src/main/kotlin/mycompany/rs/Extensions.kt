package mycompany.rs

import software.amazon.awssdk.services.dynamodb.model.AttributeValue
import mycompany.rs.DynamoDBField.*
import java.time.Instant

fun List<Map<String, AttributeValue>>.asCommonSettings() = this
        .map { it.asCommonSetting() }

fun Map<String, AttributeValue>.asCommonSetting() = CommonSetting(
        identifier = this[IDENTIFIER].asS(),
        namespace = this[NAMESPACE].asS(),
        value = this[VALUE].asS(),
        lastUpdated = this[LAST_UPDATED].asInstant()
)
fun Map<String, AttributeValue>?.asScroll(): String? = this.takeIf { it != null }?.let {
    this?.get(NAMESPACE)?.asS()
}

fun CommonSetting.asItemMap() = mapOf(
        *identifier.asAttribute(IDENTIFIER),
        *namespace.asAttribute(NAMESPACE),
        *value.asAttribute(VALUE),
        *lastUpdated.asAttribute(LAST_UPDATED)
)

fun SearchObject.asKeyMap() = mapOf(
        *identifier.asAttribute(IDENTIFIER),
        *namespace.asAttribute(NAMESPACE)
)

fun String?.obfuscate() = this.takeIf { it != null }?.toString()?.let {"${it.take(4)}..${it.takeLast(4)}" }

fun Any?.asAttribute(field: DynamoDBField) =
        this.asAttributeFor(field.lowercase())

fun Any?.asAttributeFor(key: String) =
        (if(this != null) arrayOf(key to this.asSAttribute { it.toString() }) else arrayOf())

private fun AttributeValue?.asInstant() =  Instant.parse(this.asS())
private fun AttributeValue?.asS() = this?.s()!!

private fun String.slicedKey() = this.split(":").let{ entry -> entry.first() to entry.last() }

private fun <T> T.asSAttribute(transform: (T) -> String): AttributeValue =
        AttributeValue.builder().s(transform.invoke(this)).build()

private operator fun Map<String, AttributeValue>.get(field: DynamoDBField) = this[field.lowercase()]

fun <F, S, R> withPair(first: F, second: S, block: (F, S) -> R): R  = block(first, second)

