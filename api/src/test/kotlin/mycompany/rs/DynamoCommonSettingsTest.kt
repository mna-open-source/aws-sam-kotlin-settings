package mycompany.rs

import com.nhaarman.mockitokotlin2.whenever
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.containsInAnyOrder
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import mycompany.rs.DynamoDbTestSupport.commonSettingsTable
import java.time.Clock
import java.time.Instant
import kotlin.test.assertEquals
import kotlin.test.assertNull

@RunWith(MockitoJUnitRunner.StrictStubs::class)
class DynamoCommonSettingsTest {

    companion object {
        private const val table = commonSettingsTable
        private val dynamo = createCommonSettingsTable()
    }

    @Mock
    private lateinit var clock: Clock

    private lateinit var sut: CommonSettings

    @Before
    fun setup() {
        sut = DynamoCommonSettings(
                clock = clock,
                dynamo = dynamo,
                table = table
        )
    }

    @Test
    fun `can retrieve a common setting by search object`() {
        val expected = anySavedCommonSetting()

        assertEquals(
                expected, sut.findBy(expected.asSearchObject())
        )
    }

    @Test
    fun `can retrieve a common setting by search object with composed key`() {
        val singleKey = newString()
        val compositeKey = "$singleKey!!${newString()}"
        val expected = anySavedCommonSetting(key = compositeKey)

        assertEquals(
                expected, sut.queryBy(SearchObject(expected.identifier, singleKey)).first()
        )
    }

    @Test
    fun `can retrieve a common setting list by search object with composed key`() {
        val singleKey = newString()
        val identifier = newString()
        val compositeKey = "$singleKey!!${newString()}"
        val expected = listOf(
                anySavedCommonSetting(identifier = identifier, key = compositeKey),
                anySavedCommonSetting(identifier = identifier, key = "$singleKey!!${newString()}")
        )

        assertThat(
                sut.queryBy(SearchObject(identifier, singleKey)),
                containsInAnyOrder(*expected.toTypedArray())
        )
    }

    @Test
    fun `returns null on not found common setting`() {
        /*any saved common setting */anySavedCommonSetting()

        assertNull(
                sut.findBy(anyCommonSetting().asSearchObject())
        )
    }

    @Test
    fun `can retrieve a common setting list by identifier`() {
        val singleKey = newString()
        val identifier = newString()
        val compositeKey = "$singleKey!!${newString()}"

        val settings = listOf(
                anySavedCommonSetting(identifier = identifier, key = compositeKey),
                anySavedCommonSetting(identifier = identifier, key = "$singleKey!!${newString()}")
        ).sortedBy { it.namespace }

        /*any saved common setting with different identifier*/anySavedCommonSetting()
        val expected = PagedCommonSettings(
                identifier = identifier,
                settings = settings
        )

        assertEquals(
                expected, sut.findByIdentifier(identifier, null)
        )
    }

    @Test
    fun `can retrieve a common paged setting list by identifier`() {
        val singleKey = newString()
        val identifier = newString()
        val compositeKey = "$singleKey!!${newString()}"

        val settings = listOf(
                anySavedCommonSetting(identifier = identifier, key = compositeKey),
                anySavedCommonSetting(identifier = identifier, key = "$singleKey!!${newString()}")
        ).sortedBy { it.namespace }

        /*any saved common setting with different identifier*/anySavedCommonSetting()
        val expected = PagedCommonSettings(
                identifier = identifier,
                settings = listOf(settings.last())
        )

        val scroll = settings.first().namespace

        assertEquals(
                expected, sut.findByIdentifier(identifier, scroll)
        )
    }

    @Test
    fun `returns empty on not found common setting by identifier`() {
        /*any saved common setting */anySavedCommonSetting()
        val identifier = newString()

        assertEquals(
                PagedCommonSettings(identifier), sut.findByIdentifier(identifier, null)
        )
    }

    private fun anySavedCommonSetting(identifier: String = newString(), key: String = newString()) = anyCommonSetting(identifier, key)
            .let {
                anyInstant()
                sut.save(it)
            }

    private fun anyInstant() = Instant.now().also {
        whenever(clock.instant())
                .thenReturn(it)
    }
}
