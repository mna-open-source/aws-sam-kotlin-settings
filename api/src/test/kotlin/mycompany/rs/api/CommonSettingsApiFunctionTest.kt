package mycompany.rs.api

import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent
import com.nhaarman.mockitokotlin2.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import mycompany.rs.*
import mycompany.rs.TestExtensions.mapper
import mycompany.rs.api.CommonSettingsApiFunction.Companion.basePath
import mycompany.rs.api.CommonSettingsApiFunction.Companion.findByIdentifier
import kotlin.test.assertEquals

@RunWith(MockitoJUnitRunner.StrictStubs::class)
class CommonSettingsApiFunctionTest {
    @Mock
    private lateinit var apiRouter: ApiRouter

    @Mock
    private lateinit var exceptionHandler: ExceptionHandler

    @Mock
    private lateinit var context: Context

    private lateinit var sut: CommonSettingsApiFunction

    @Before
    fun setup() {
        sut = CommonSettingsApiFunction(
            apiRouter = apiRouter,
            exceptionHandler = exceptionHandler,
            mapper = mapper()
        )
    }

    @Test
    fun `fails to route events without authorization header`() {
        val response = accessDenied()

        whenever(exceptionHandler.handle(any<MissingAuthorizationHeader>()))
            .thenReturn(response)

        assertEquals(
            response,
            sut.handleRequest(anyPostRequestEvent(authorization = null), context)
        )

        verifyZeroInteractions(apiRouter)
    }

    @Test
    fun `fails to route events for unhandled paths`() {
        val invalidPath = "invalid_path"

        assertEquals(
            notFound(invalidPath),
            sut.handleRequest(
                anyGetRequestEvent(path = invalidPath),
                context
            )
        )
    }

    @Test
    fun `delegates to exception handler when an exception is thrown during handling`() {
        val commonSetting = anyCommonSetting()
        val authorization = newString()
        val exception = RuntimeException()
        val request = anySaveRequest(authorization, mapper().writeValueAsString(commonSetting))
        val expected = badRequest(newString())

        whenever(apiRouter.save(commonSetting, authorization))
            .thenThrow(exception)

        whenever(exceptionHandler.handle(exception))
            .thenReturn(expected)

        assertEquals(
            expected,
            sut.handleRequest(request, context)
        )
    }

    @Test
    fun `returns BAD_REQUEST on invalid save events`() {
        val body = "{}"
        val expected = badRequest(
            """
                    {"errors":[{"entity":"request","property":"body","invalidValue":"","message":"missing parameter: identifier"}]}
                """.trimIndent()
        )

        whenever(exceptionHandler.handle(any<IllegalArgumentException>()))
            .thenReturn(expected)

        val actual = sut.handleRequest(
            anySaveRequest(
                body = body,
                authorization = newString()
            ), context
        )

        assertEquals(expected, actual)
    }

    @Test
    fun `returns BAD_REQUEST on missing body on invalid save events`() {
        val body = null

        val expected = badRequest(
            """
                    {"errors":[{"entity":"request","property":"body","invalidValue":"","message":"missing body"}]}
                """.trimIndent()
        )

        val actual = sut.handleRequest(
            anySaveRequest(
                body = body,
                authorization = newString()
            ), context
        )

        assertEquals(expected, actual)
    }

    @Test
    fun `returns BAD_REQUEST on missing query parameters on invalid find events`() {
        val expected = badRequest(
            """
                    {"errors":[{"entity":"request","property":"query parameters","invalidValue":"","message":"missing query parameters"}]}
                """.trimIndent()
        )

        whenever(exceptionHandler.handle(any<MissingQueryParams>()))
            .thenReturn(expected)

        val actual = sut.handleRequest(
            anyGetRequestEvent(), context
        )

        assertEquals(expected, actual)
    }

    @Test
    fun `returns BAD_REQUEST on missing query parameter on invalid find events`() {
        val expected = badRequest(
            """
                    {"errors":[{"entity":"request","property":"key","invalidValue":"","message":"missing parameter: key"}]}
                """.trimIndent()
        )

        whenever(exceptionHandler.handle(any<MissingQueryString>()))
            .thenReturn(expected)

        val actual = sut.handleRequest(
            anyGetRequestEvent(params = mapOf("identifier" to newString())), context
        )

        assertEquals(expected, actual)
    }

    @Test
    fun `can route find common setting from search object events`() {
        val (setting, search) = anySearchObjectWithSetting()

        val params = mapOf(
            "identifier" to search.identifier,
            "namespace" to search.namespace
        )

        whenever(apiRouter.findSetting(search))
            .thenReturn(setting)

        val actual = sut.handleRequest(
            anyGetRequestEvent(params = params), context
        )

        assertEquals(ok(setting.asResponseBody()), actual)
    }

    @Test
    fun `can route find common setting from search object events and returns not found`() {
        val (_, search) = anySearchObjectWithSetting()

        val params = mapOf(
            "identifier" to search.identifier,
            "namespace" to search.namespace
        )
        val event = anyGetRequestEvent(params = params)

        whenever(apiRouter.findSetting(search))
            .thenReturn(null)

        val actual = sut.handleRequest(event, context)

        assertEquals(notFound("${event.path} - ${event.queryStringParameters}"), actual)
    }

    @Test
    fun `given a payload can save a common setting`() {
        val authorization = newString()

        val body = """
                     {"identifier":"TAX_INCLUDED","namespace":"PRICE_LIST_ID!!4d2f8702-4abc-44f6-a735-2e599195d866","value":"true","lastUpdated":null}
                """.trimIndent()

        val event = anySaveRequest(
            authorization, body
        )

        val setting = CommonSetting(
            identifier = "TAX_INCLUDED",
            namespace = "PRICE_LIST_ID!!4d2f8702-4abc-44f6-a735-2e599195d866",
            value = "true"
        )

        whenever(apiRouter.save(setting, authorization))
            .thenReturn(setting)

        assertEquals(ok(body), sut.handleRequest(event, context))
    }

    @Test
    fun `can route find paged common settings from identifier and no scroll events`() {
        val settings = anyPagedCommonSettings()

        val params = mapOf(
            "identifier" to settings.identifier
        )

        whenever(apiRouter.findByIdentifier(settings.identifier, null))
            .thenReturn(settings)

        val actual = sut.handleRequest(
            anyGetRequestEvent(path = findByIdentifier, params = params), context
        )

        assertEquals(ok(settings.asResponseBody()), actual)
    }

    @Test
    fun `can route find paged common settings from identifier and not null scroll events`() {
        val settings = anyPagedCommonSettings(newString())
        val params = mapOf(
            "identifier" to settings.identifier,
            "scroll" to settings.scroll!!
        )

        whenever(apiRouter.findByIdentifier(settings.identifier, settings.scroll))
            .thenReturn(settings)

        val actual = sut.handleRequest(
            anyGetRequestEvent(path = findByIdentifier, params = params), context
        )

        assertEquals(ok(settings.asResponseBody()), actual)
    }

    private fun anySaveRequest(authorization: String, body: String?) =
        anyPostRequestEvent(authorization = authorization)
            .withBody(body)

    private fun anyGetRequestEvent(path: String = basePath, params: Map<String, String> = mapOf()) =
        anyRequestEvent(path = path, method = "GET").apply {
            if (params.isNotEmpty()) withQueryStringParameters(params)
        }

    private fun anyPostRequestEvent(authorization: String? = newString(), path: String = basePath) =
        anyRequestEvent(authorization, path, "POST")

    private fun anyRequestEvent(authorization: String? = newString(), path: String = basePath, method: String) =
        APIGatewayProxyRequestEvent()
            .withHttpMethod(method)
            .withPath(path).apply {
                if (authorization != null) withHeaders(mapOf("Authorization" to "Bearer $authorization"))
            }

    private fun anySearchObjectWithSetting() = anyCommonSetting().let {
        it to it.asSearchObject()
    }

    private fun anyPagedCommonSettings(scroll: String? = null) = anyCommonSetting().let {
        PagedCommonSettings(
            it.identifier, listOf(it), scroll
        )
    }

    private fun CommonSetting.asResponseBody() = mapper().writeValueAsString(this)
    private fun PagedCommonSettings.asResponseBody() = mapper().writeValueAsString(this)

}
