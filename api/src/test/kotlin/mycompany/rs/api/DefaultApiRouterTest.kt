package mycompany.rs.api

import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyZeroInteractions
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import mycompany.rs.*
import java.time.Instant
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

@RunWith(MockitoJUnitRunner.StrictStubs::class)
class DefaultApiRouterTest {

    @Mock
    private lateinit var commonSettings: CommonSettings

    @Mock
    private lateinit var security: Security

    private lateinit var sut: ApiRouter

    @Before
    fun setup() {
        sut = DefaultApiRouter(
            commonSettings = commonSettings,
            security = security
        )
    }

    @Test
    fun `can save a common setting`() {
        val (commonSetting, authorization) = anySaveParameters()
        val expected = commonSetting.copy(lastUpdated = Instant.now())

        whenever(commonSettings.save(commonSetting))
            .thenReturn(expected)

        assertEquals(
            expected, sut.save(commonSetting, authorization)
        )

        verify(security).checkAllowedToSave(authorization)
    }

    @Test
    fun `fails on invalid authorization`() {
        val (commonSetting, authorization) = anySaveParameters()

        whenever(security.checkAllowedToSave(authorization))
            .thenThrow(Forbidden("any validation message"))

        assertFailsWith<Forbidden> {
            sut.save(commonSetting, authorization)
        }

        verify(security).checkAllowedToSave(authorization)
        verifyZeroInteractions(commonSettings)
    }

    @Test
    fun `can retrieve a common setting given a search object`() {
        val expected = anyCommonSetting()
        val searchObject = expected.asSearchObject()

        whenever(commonSettings.findBy(searchObject))
            .thenReturn(expected)

        assertEquals(
            expected, sut.findSetting(searchObject)
        )

        verifyZeroInteractions(security)
    }

    @Test
    fun `can retrieve a common setting given a identifier`() {
        val singleCommonSetting = anyCommonSetting()
        val identifier = singleCommonSetting.identifier
        val expected = PagedCommonSettings(
            identifier = identifier,
            settings = listOf(singleCommonSetting)
        )
        val scroll = newString()

        whenever(commonSettings.findByIdentifier(expected.identifier, scroll))
            .thenReturn(expected)

        assertEquals(
            expected, sut.findByIdentifier(identifier, scroll)
        )

        verifyZeroInteractions(security)
    }

    private fun anySaveParameters() =
        anyCommonSetting() to newString()
}
