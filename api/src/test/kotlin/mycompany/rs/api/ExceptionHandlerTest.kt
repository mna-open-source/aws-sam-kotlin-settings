package mycompany.rs.api

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.junit.Before
import org.junit.Test
import mycompany.rs.Forbidden
import mycompany.rs.newString
import kotlin.test.assertEquals

class ExceptionHandlerTest {

    companion object {
        private val mapper = jacksonObjectMapper()
    }

    private lateinit var sut: ExceptionHandler

    @Before
    fun setup() {
        sut = DefaultExceptionHandler(mapper)
    }

    @Test
    fun `returns accessDenied on handling MissingAuthorizationHeader`() {
        val expected = accessDenied()

        assertEquals(
            expected, sut.handle(MissingAuthorizationHeader())
        )
    }

    @Test
    fun `returns accessDenied on handling InvalidToken`() {
        val expected = accessDenied()

        assertEquals(
            expected, sut.handle(InvalidToken(newString()))
        )
    }

    @Test
    fun `returns accessDenied on handling Unauthorized`() {
        val expected = accessDenied()

        assertEquals(
            expected, sut.handle(MissingAuthorizationHeader())
        )
    }

    @Test
    fun `returns forbidden on handling Forbidden`() {
        val message = newString()
        val expected = forbidden(message)

        assertEquals(
            expected, sut.handle(Forbidden(message))
        )
    }

    @Test
    fun `returns badRequest on handling MissingQueryString`() {
        val parameter = newString()
        val thrown = MissingQueryString(parameter)
        val expected = badRequest(thrown.let { it ->
            ErrorResponse(
                setOf(
                    ErrorWrapper(
                        entity = "request",
                        property = it.parameter,
                        invalidValue = "",
                        message = it.message!!
                    )
                )
            ).let {
                mapper.writeValueAsString(it)
            }
        })

        assertEquals(
            expected, sut.handle(thrown)
        )
    }

    @Test
    fun `returns notFound on handling RatingNotFound`() {
        val message = newString()
        val expected = notFound(message)

        assertEquals(
            expected, sut.handle(NotFound(message))
        )
    }

    @Test
    fun `returns internalServerError on any throwable`() {
        val message = newString()
        val expected = internalServerError(message)

        assertEquals(
            expected, sut.handle(RuntimeException(message))
        )
    }
}
