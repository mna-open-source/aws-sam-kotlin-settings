package mycompany.rs

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import java.lang.IllegalArgumentException
import kotlin.test.assertFailsWith

@RunWith(MockitoJUnitRunner.StrictStubs::class)
class DefaultSecurityTest {

    @Mock
    private lateinit var tokenDecoder: TokenDecoder

    private lateinit var sut: Security

    @Before
    fun setup() {
        sut = DefaultSecurity(
            tokenDecoder = tokenDecoder
        )
    }

    @Test
    fun `fails for not found authorities`() {
        val token = newString()

        whenever(tokenDecoder.decode(token))
            .thenReturn(tokenFor())

        assertFailsWith<Forbidden> {
            sut.checkAllowedToSave(token)
        }

        verify(tokenDecoder).decode(token)
    }

    @Test
    fun `does nothing for a valid token`() {
        val token = newString()

        whenever(tokenDecoder.decode(token))
            .thenReturn(tokenFor(listOf("STORES_SAVE_PRICE_LIST", newString())))

        sut.checkAllowedToSave(token)

        verify(tokenDecoder).decode(token)
    }

    private fun tokenFor(authorities: List<String> = listOf(newString(), newString())) = "secret".let { secret ->
        withPair(
            generateCredentials(secret, authorities),
            Algorithm.HMAC256(secret)
        ) { token, algorithm ->
            token.appendToTuple {
                JWT.require(algorithm)
                    .withIssuer(DefaultSecurity.issuer)
                    .build()
            }.tupleLet { t, verifier ->
                verifier.verify(t)

            }

        }
    }
}
