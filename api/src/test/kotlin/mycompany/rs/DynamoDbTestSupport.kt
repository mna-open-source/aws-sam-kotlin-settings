package mycompany.rs

import mycompany.rs.DynamoDbTestSupport.commonSettingsTable
import org.junit.AssumptionViolatedException
import org.testcontainers.containers.ContainerLaunchException
import org.testcontainers.containers.localstack.LocalStackContainer
import org.testcontainers.utility.DockerImageName
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider
import software.amazon.awssdk.regions.Region
import software.amazon.awssdk.services.dynamodb.DynamoDbClient
import software.amazon.awssdk.services.dynamodb.model.*
import java.net.URI


lateinit var cachedDynamoClient: DynamoDbClient
lateinit var cachedContainer: LocalStackContainer

object DynamoDbTestSupport {
    const val commonSettingsTable = "table"
}

fun createCommonSettingsTable(): DynamoDbClient =
    initClient(block = ::createCommonSettingTable)

private fun createCommonSettingTable(client: DynamoDbClient) {
    val createTableRequest = CreateTableRequest
        .builder()
        .tableName(commonSettingsTable)
        .attributeDefinitions(
            AttributeDefinition
                .builder()
                .attributeName(DynamoDBField.IDENTIFIER.lowercase())
                .attributeType(ScalarAttributeType.S).build(),
            AttributeDefinition
                .builder()
                .attributeName(DynamoDBField.NAMESPACE.lowercase())
                .attributeType(ScalarAttributeType.S).build()
        )
        .billingMode(BillingMode.PAY_PER_REQUEST)
        .provisionedThroughput(ProvisionedThroughput.builder().readCapacityUnits(100).writeCapacityUnits(100).build())
        .keySchema(
            KeySchemaElement.builder().attributeName(DynamoDBField.IDENTIFIER.lowercase()).keyType(KeyType.HASH)
                .build(),
            KeySchemaElement.builder().attributeName(DynamoDBField.NAMESPACE.lowercase()).keyType(KeyType.RANGE).build()
        )
        .build()
    client.createTable(createTableRequest)
    waitForTableToBeCreated(client)
}

private fun initContainer(): LocalStackContainer =
    if (!::cachedContainer.isInitialized) {
        val localstackImage = DockerImageName.parse("localstack/localstack:latest")

        LocalStackContainer(localstackImage)
            .withServices(LocalStackContainer.Service.DYNAMODB)
            .apply { start() }
            .also { cachedContainer = it }
    } else {
        cachedContainer
    }

private fun initClient() =
    if (!::cachedDynamoClient.isInitialized) {
        val port = 4566

        val container = initContainer()

        DynamoDbClient
            .builder()
            .endpointOverride(URI.create("http://${container.containerIpAddress}:${container.getMappedPort(port)}"))
            .credentialsProvider(StaticCredentialsProvider.create(AwsBasicCredentials.create("dummy", "dummy")))
            .region(Region.US_EAST_2)
            .build()
    } else {
        cachedDynamoClient
    }

private fun initClient(blockName: String = "commonSettingsTable", block: (DynamoDbClient) -> Unit) =
    try {
        initClient().also {
            block(it)
        }
    } catch (e: ContainerLaunchException) {
        throw AssumptionViolatedException("cannot init $blockName table because: ${e.message}", e)
    }

private fun waitForTableToBeCreated(client: DynamoDbClient, table: String = commonSettingsTable) {
    (1..3).find {
        val status = client.describeTable(DescribeTableRequest.builder().tableName(table).build()).table().tableStatus()
        val isReady = status == TableStatus.ACTIVE
        if (!isReady) Thread.sleep(500)
        isReady
    } ?: throw IllegalStateException("table $table is not ready yet")
}
