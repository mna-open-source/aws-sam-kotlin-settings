package mycompany.rs

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import java.time.Instant
import java.util.*

object TestExtensions {
    fun mapper() = jacksonObjectMapper()
}

fun anyCommonSetting(
    identifier: String = newString(),
    key: String = newString(),
    now: Instant? = null
) = CommonSetting(
    identifier = identifier,
    namespace = key,
    value = newString(),
    lastUpdated = now
)

fun newString() = UUID.randomUUID().toString()

fun CommonSetting.asSearchObject() = SearchObject(
    identifier = identifier,
    namespace = namespace
)
