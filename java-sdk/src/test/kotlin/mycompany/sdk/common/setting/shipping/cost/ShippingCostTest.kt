package mycompany.sdk.common.setting.shipping.cost

import org.junit.Test
import java.math.BigDecimal
import kotlin.test.assertFailsWith
import kotlin.test.assertNotNull

class ShippingCostTest {

    companion object {
        private const val currency = "ARS"
    }

    @Test
    fun `can set an amount greater than zero`() {
        val amount = BigDecimal.TEN

        assertNotNull(
                Money(amount, currency)
        )
    }

    @Test
    fun `can set an amount equals to zero`() {
        val amount = BigDecimal.ZERO

        assertNotNull(
                Money(amount, currency)
        )
    }

    @Test
    fun `fails on set amount smaller than zero`() {
        val amount = BigDecimal.ONE.negate()

        assertFailsWith<IllegalArgumentException> {
            Money(amount, currency)
        }
    }
}
