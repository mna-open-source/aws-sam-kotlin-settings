package mycompany.sdk.common.setting

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import reactor.core.publisher.Mono
import mycompany.sdk.common.setting.price.MinimumPurchasePrice
import mycompany.sdk.common.setting.price.MinimumPurchasePrices
import mycompany.sdk.common.setting.shipping.cost.Money
import mycompany.sdk.common.setting.shipping.cost.ShippingCost
import mycompany.sdk.common.setting.shipping.cost.ShippingCosts
import mycompany.sdk.common.setting.tax.TaxSetting
import mycompany.sdk.common.setting.tax.TaxSettings
import java.math.BigDecimal
import java.util.*

object TestExtensions {
    fun mapper() = jacksonObjectMapper()
}

fun anyMinimumPurchasePrice() = MinimumPurchasePrice(
    priceListIdentifier = newString(),
    channelIdentifier = newString(),
    price = BigDecimal.TEN
)

fun anyMinimumPurchasePrices(scroll: String? = null) = MinimumPurchasePrices(
    prices = listOf(anyMinimumPurchasePrice(), anyMinimumPurchasePrice()),
    scroll = scroll
)

fun anyMoney() = Money(
    currency = "ARS",
    amount = BigDecimal.TEN
)

fun anyShippingCost() = ShippingCost(
    priceListIdentifier = newString(),
    channelIdentifier = newString(),
    value = anyMoney()
)

fun anyShippingCosts(scroll: String? = null) = ShippingCosts(
    elements = listOf(anyShippingCost(), anyShippingCost()),
    scroll = scroll
)

fun anyTaxSetting(isTaxIncluded: Boolean = true) = TaxSetting(
    priceListIdentifier = newString(),
    isTaxIncluded = isTaxIncluded
)

fun anyTaxSettings(scroll: String? = null) = TaxSettings(
    settings = listOf(anyTaxSetting(), anyTaxSetting()),
    scroll = scroll
)

fun newString() = UUID.randomUUID().toString()

fun MockWebServer.givenResponseBody(response: String = "", code: Int = 200) {
    enqueue(
        MockResponse()
            .setResponseCode(code)
            .setBody(response)
            .addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
    )
}

fun MockWebServer.givenAnErrorResponse(code: Int = 404) {
    enqueue(
        MockResponse().setResponseCode(code)
    )
}

fun <T> Mono<T>.checkedBlock(): T = this
    .blockOptional()
    .orElseThrow { AssertionError() }

fun initMockServer(): Triple<MockWebServer, String, Int> {
    val defaultPath = "http://localhost:%s"
    val mockserver = MockWebServer()
    mockserver.start()
    val port = mockserver.port
    return Triple(mockserver, defaultPath, port)
}
