package mycompany.sdk.common.setting.tax

import okhttp3.mockwebserver.MockWebServer
import org.junit.AssumptionViolatedException
import org.junit.Before
import org.junit.Test
import org.springframework.web.reactive.function.client.WebClient
import reactor.test.StepVerifier
import mycompany.sdk.common.setting.*
import mycompany.sdk.common.setting.TestExtensions.mapper
import kotlin.test.assertEquals
import kotlin.test.assertFalse

sealed class TaxSettingsSdkTest {

    companion object {
        val webClient: WebClient.Builder = WebClient.builder()
    }

    lateinit var sut: TaxSettingsSdk

    private lateinit var token: String

    abstract fun create(): HttpTaxSettingsSdk
    abstract fun token(): String
    abstract fun anySavedTaxSettingFor(value: Boolean): String
    abstract fun anyNotFoundSettingFor(): String
    abstract fun anySavedTaxSettings(): TaxSettings

    @Before
    fun setup() {
        sut = create()
        token = token()
    }

    @Test
    fun `can retrieve true value from sdk for found tax setting`() {
        val expected = true
        val priceListIdentifier = anySavedTaxSettingFor(expected)

        StepVerifier.create(
            sut.isTaxIncludedOn(priceListIdentifier)
        ).assertNext {
            assertEquals(expected, it)
        }.verifyComplete()
    }

    @Test
    fun `can retrieve false value from sdk for found tax setting`() {
        val expected = false
        val priceListIdentifier = anySavedTaxSettingFor(expected)

        StepVerifier.create(
            sut.isTaxIncludedOn(priceListIdentifier)
        ).assertNext {
            assertEquals(expected, it)
        }.verifyComplete()
    }

    @Test
    fun `handle not found setting and returns as false value`() {
        val priceListIdentifier = anyNotFoundSettingFor()

        StepVerifier.create(
            sut.isTaxIncludedOn(priceListIdentifier)
        ).assertNext {
            assertFalse(it)
        }.verifyComplete()
    }

    @Test
    fun `can retrieve tax settings`() {
        val expected = anySavedTaxSettings()

        StepVerifier.create(
            sut.findAll()
        ).assertNext {
            assertEquals(expected, it)
        }.verifyComplete()
    }
}

class MockWebServerTaxSettingsSdkTest : TaxSettingsSdkTest() {

    companion object {
        private lateinit var mockServer: MockWebServer
    }

    override fun create(): HttpTaxSettingsSdk = initMockServer().let {
        mockServer = it.first
        HttpTaxSettingsSdk.Builder(webClient)
            .path(String.format(it.second, it.third))
            .build()
    }

    override fun token() = newString()

    override fun anySavedTaxSettingFor(value: Boolean): String = newString()
        .also { mockServer.givenResponseBody(mapper().writeValueAsString(anyTaxSetting(value).asCommonSetting())) }

    override fun anyNotFoundSettingFor(): String = newString()
        .also { mockServer.givenAnErrorResponse() }

    override fun anySavedTaxSettings() = anyTaxSettings().also {
        mockServer.givenResponseBody(mapper().writeValueAsString(it.asCommonSettings()))
    }

    private fun TaxSettings.asCommonSettings() = CommonSettings(
        identifier = Identifier.TAX_SETTING,
        settings = this.settings.map { it.asCommonSetting() },
        scroll = scroll
    )
}

class SamLocalHttpTaxSettingsSdkTest : TaxSettingsSdkTest() {

    companion object {
        private const val key = "TEST_ACCESS_TOKEN"
        private const val samLocalPath = "http://localhost:3000"
    }

    private lateinit var testAccessToken: String

    override fun create(): HttpTaxSettingsSdk =
        getAccessToken().takeIf { it != null }
            ?.let(::initialize)
            ?: throw AssumptionViolatedException("$key is not set in test environment")

    override fun token() = testAccessToken

    override fun anySavedTaxSettingFor(value: Boolean): String =
        saveAnyTaxSetting(value).priceListIdentifier

    override fun anyNotFoundSettingFor(): String = newString().also {
        saveAnyTaxSetting(true)
    }

    override fun anySavedTaxSettings(): TaxSettings = anyTaxSettings().also {
        it.settings.map { p ->
            sut.save(p, testAccessToken).checkedBlock()
        }
    }

    @Test
    fun `can retrieve minimum purchase prices with scroll`() {
        val saved = anySavedTaxSettings()
        val taken =
            saved.settings.map { it.asCommonSetting() }.sortedBy { it.namespace }.map { it.asTaxSetting() }.takeLast(2)
        val scroll = taken.first()
        val expected = TaxSettings(
            settings = taken.takeLast(1),
            scroll = null
        )

        StepVerifier.create(
            sut.findAll(scroll.asCommonSetting().namespace)
        ).assertNext {
            assertEquals(expected, it)
        }.verifyComplete()
    }

    private fun saveAnyTaxSetting(value: Boolean) = anyTaxSetting(value).let {
        sut.save(it, testAccessToken).checkedBlock()
    }

    private fun initialize(accessToken: String): HttpTaxSettingsSdk =
        HttpTaxSettingsSdk.Builder(webClient)
            .path(samLocalPath)
            .build()
            .also {
                testAccessToken = accessToken
                println("starting local execution with ${testAccessToken.obfuscate()}")
            }

    private fun getAccessToken() = System.getenv(key)

    private fun String.obfuscate() = "${this.take(4)}..${this.takeLast(4)}"
}
