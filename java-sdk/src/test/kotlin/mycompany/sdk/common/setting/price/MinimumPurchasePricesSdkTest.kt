package mycompany.sdk.common.setting.price

import okhttp3.mockwebserver.MockWebServer
import org.junit.AssumptionViolatedException
import org.junit.Before
import org.junit.Test
import org.springframework.web.reactive.function.client.WebClient
import reactor.test.StepVerifier
import mycompany.sdk.NotFound
import mycompany.sdk.common.setting.*
import mycompany.sdk.common.setting.TestExtensions.mapper
import java.math.BigDecimal
import kotlin.test.assertEquals

sealed class MinimumPurchasePricesSdkTest {

    companion object {
        val webClient: WebClient.Builder = WebClient.builder()
    }

    lateinit var sut: MinimumPurchasePricesSdk

    private lateinit var token: String

    abstract fun create(): HttpMinimumPurchasePricesSdk
    abstract fun token(): String
    abstract fun anySavedMinimumPurchase(): Pair<BigDecimal, PriceListChannelSearch>
    abstract fun anySavedMinimumPurchasePrices(): MinimumPurchasePrices
    abstract fun notFoundMinimumPurchasePrice(): PriceListChannelSearch

    @Before
    fun setup() {
        sut = create()
        token = token()
    }

    @Test
    fun `can retrieve minimum purchase price`() {
        val (expected, searchObject) = anySavedMinimumPurchase()

        StepVerifier.create(
            sut.findBy(searchObject)
        ).assertNext {
            assertEquals(expected, it)
        }.verifyComplete()
    }

    @Test
    fun `can retrieve minimum purchase prices`() {
        val expected = anySavedMinimumPurchasePrices()

        StepVerifier.create(
            sut.findAll()
        ).assertNext {
            assertEquals(expected, it)
        }.verifyComplete()
    }

    @Test
    fun `fails for not found minimum purchase price`() {
        val searchObject = notFoundMinimumPurchasePrice()

        StepVerifier.create(
            sut.findBy(searchObject)
        ).expectError(NotFound::class.java).verify()
    }
}

class MockWebServerHttpMinimumPurchasePricesSdkTestTest : MinimumPurchasePricesSdkTest() {

    companion object {
        private lateinit var mockServer: MockWebServer
        private const val defaultPath = "http://localhost:%s"
    }

    private var port = 0

    override fun create(): HttpMinimumPurchasePricesSdk =
        MockWebServer().also { server ->
            mockServer = server
            mockServer.start()
            port = mockServer.port
        }.let {
            HttpMinimumPurchasePricesSdk.Builder(webClient)
                .path(String.format(defaultPath, port))
                .build()
        }

    override fun token() = newString()

    override fun anySavedMinimumPurchase(): Pair<BigDecimal, PriceListChannelSearch> =
        anyMinimumPurchasePrice().let {
            mockServer.givenResponseBody(mapper().writeValueAsString(it.asCommonSetting()))
            it.price to PriceListChannelSearch(
                priceListIdentifier = it.priceListIdentifier,
                channelIdentifier = it.priceListIdentifier
            )
        }

    override fun anySavedMinimumPurchasePrices(): MinimumPurchasePrices =
        anyMinimumPurchasePrices().also {
            mockServer.givenResponseBody(mapper().writeValueAsString(it.asCommonSettings()))
        }

    override fun notFoundMinimumPurchasePrice(): PriceListChannelSearch =
        anyMinimumPurchasePrice().let {
            mockServer.givenAnErrorResponse()
            PriceListChannelSearch(
                priceListIdentifier = it.priceListIdentifier,
                channelIdentifier = it.priceListIdentifier
            )
        }

    private fun MinimumPurchasePrices.asCommonSettings() =
        CommonSettings(
            identifier = Identifier.MINIMUM_PURCHASE_PRICE,
            settings = this.prices.map { it.asCommonSetting() },
            scroll = scroll
        )
}

class SamLocalHttpMinimumPurchasePricesSdkTest : MinimumPurchasePricesSdkTest() {

    companion object {
        private const val key = "TEST_ACCESS_TOKEN"
        private const val samLocalPath = "http://localhost:3000"
    }

    private lateinit var testAccessToken: String

    override fun create(): HttpMinimumPurchasePricesSdk =
        getAccessToken().takeIf { it != null }
            ?.let(::initialize)
            ?: throw AssumptionViolatedException("$key is not set in test environment")

    override fun token() = testAccessToken

    override fun anySavedMinimumPurchase(): Pair<BigDecimal, PriceListChannelSearch> =
        anyMinimumPurchasePrice().let {
            val saved = saveAnyMinimumPurchasePrice(it)
            saved.price to PriceListChannelSearch(
                priceListIdentifier = saved.priceListIdentifier,
                channelIdentifier = saved.channelIdentifier
            )
        }

    override fun anySavedMinimumPurchasePrices(): MinimumPurchasePrices =
        anyMinimumPurchasePrices().also {
            it.prices.map { p ->
                sut.save(p, testAccessToken).checkedBlock()
            }
        }

    override fun notFoundMinimumPurchasePrice() = PriceListChannelSearch(
        priceListIdentifier = newString(),
        channelIdentifier = newString()
    )

    @Test
    fun `can retrieve minimum purchase prices with scroll`() {
        val saved = anySavedMinimumPurchasePrices()
        val taken =
            saved.prices.map { it.asCommonSetting() }.sortedBy { it.namespace }.map { it.asMinimumPurchasePrice() }
                .takeLast(2)
        val scroll = taken.first()

        val expected = MinimumPurchasePrices(
            prices = taken.takeLast(1),
            scroll = null
        )

        StepVerifier.create(
            sut.findAll(scroll.asCommonSetting().namespace)
        ).assertNext {
            assertEquals(expected, it)
        }.verifyComplete()
    }

    private fun saveAnyMinimumPurchasePrice(minimumPurchasePrice: MinimumPurchasePrice) =
        sut.save(minimumPurchasePrice, testAccessToken)
            .checkedBlock()

    private fun initialize(accessToken: String): HttpMinimumPurchasePricesSdk =
        HttpMinimumPurchasePricesSdk.Builder(webClient)
            .path(samLocalPath)
            .build()
            .also {
                testAccessToken = accessToken
                println("starting local execution with ${testAccessToken.obfuscate()}")
            }

    private fun getAccessToken() = System.getenv(key)

    private fun String.obfuscate() = "${this.take(4)}..${this.takeLast(4)}"
}
