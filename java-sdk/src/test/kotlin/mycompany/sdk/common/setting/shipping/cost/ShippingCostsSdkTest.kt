package mycompany.sdk.common.setting.shipping.cost

import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.AssumptionViolatedException
import org.junit.Before
import org.junit.Test
import org.springframework.web.reactive.function.client.WebClient
import reactor.test.StepVerifier
import mycompany.sdk.NotFound
import mycompany.sdk.common.setting.*
import kotlin.test.assertEquals

sealed class ShippingCostsSdkTest {

    companion object {
        val webClient: WebClient.Builder = WebClient.builder()
    }

    lateinit var sut: ShippingCostsSdk

    private lateinit var token: String

    abstract fun create(): HttpShippingCostsSdk
    abstract fun token(): String
    abstract fun anySavedShippingCost(): Pair<ShippingCost, PriceListChannelSearch>
    abstract fun anySavedShippingCosts(): ShippingCosts
    abstract fun notFoundShippingCost(): PriceListChannelSearch

    @Before
    fun setup() {
        sut = create()
        token = token()
    }

    @Test
    fun `can retrieve shipping cost`() {
        val (expected, searchObject) = anySavedShippingCost()

        StepVerifier.create(
            sut.findBy(searchObject)
        ).assertNext {
            assertEquals(expected, it)
        }.verifyComplete()
    }

    @Test
    fun `can retrieve shipping costs`() {
        val expected = anySavedShippingCosts()

        StepVerifier.create(
            sut.findAll()
        ).assertNext {
            assertEquals(expected, it)
        }.verifyComplete()
    }

    @Test
    fun `fails for not found shipping cost`() {
        val searchObject = notFoundShippingCost()

        StepVerifier.create(
            sut.findBy(searchObject)
        ).expectError(NotFound::class.java).verify()
    }

    @Test
    fun `for found shipping cost ignores default value`() {
        val (expected, searchObject) = anySavedShippingCost()
        val default = anyShippingCost()

        StepVerifier.create(
            sut.findByOrDefault(searchObject, default)
        ).assertNext {
            assertEquals(expected, it)
        }.verifyComplete()
    }

    @Test
    fun `for not found shipping cost uses default value`() {
        val searchObject = notFoundShippingCost()
        val expected = anyShippingCost()

        StepVerifier.create(
            sut.findByOrDefault(searchObject, expected)
        ).assertNext {
            assertEquals(expected, it)
        }.verifyComplete()
    }

    @Test
    fun `for found shipping cost does not throw given exception`() {
        val (expected, searchObject) = anySavedShippingCost()
        val thrown = RuntimeException()

        StepVerifier.create(
            sut.findByOrThrow(searchObject, thrown)
        ).assertNext {
            assertEquals(expected, it)
        }.verifyComplete()
    }

    @Test
    fun `for not found shipping cost throw given exception`() {
        val searchObject = notFoundShippingCost()
        val thrown = RuntimeException()

        StepVerifier.create(
            sut.findByOrThrow(searchObject, thrown)
        ).expectError(thrown.javaClass).verify()
    }
}

class MockWebServerShippingCostsSdkTest : ShippingCostsSdkTest() {

    companion object {
        private lateinit var mockServer: MockWebServer
        private const val defaultPath = "http://localhost:%s"
    }

    private var port = 0

    override fun create(): HttpShippingCostsSdk =
        MockWebServer().also { server ->
            mockServer = server
            mockServer.start()
            port = mockServer.port
        }.let {
            HttpShippingCostsSdk.Builder(webClient)
                .path(String.format(defaultPath, port))
                .build()
        }

    override fun token() = newString()

    override fun anySavedShippingCost(): Pair<ShippingCost, PriceListChannelSearch> =
        anyShippingCost().let {
            mockServer.givenResponseBody(TestExtensions.mapper().writeValueAsString(it.toCommonSetting()))
            it to PriceListChannelSearch(
                priceListIdentifier = it.priceListIdentifier,
                channelIdentifier = it.priceListIdentifier
            )
        }

    override fun anySavedShippingCosts(): ShippingCosts =
        anyShippingCosts().also {
            mockServer.givenResponseBody(TestExtensions.mapper().writeValueAsString(it.toCommonSettings()))
        }

    override fun notFoundShippingCost(): PriceListChannelSearch =
        anyShippingCost().let {
            mockServer.givenAnErrorResponse()
            PriceListChannelSearch(
                priceListIdentifier = it.priceListIdentifier,
                channelIdentifier = it.priceListIdentifier
            )
        }

    private fun ShippingCosts.toCommonSettings() =
        CommonSettings(
            identifier = Identifier.SHIPPING_COST,
            settings = this.elements.map { it.toCommonSetting() },
            scroll = scroll
        )
}

class SamLocalHttpShippingCostsSdkTest : ShippingCostsSdkTest() {

    companion object {
        private const val key = "TEST_ACCESS_TOKEN"
        private const val samLocalPath = "http://localhost:3000"
    }

    private lateinit var testAccessToken: String

    override fun create(): HttpShippingCostsSdk =
        getAccessToken().takeIf { it != null }
            ?.let(::initialize)
            ?: throw AssumptionViolatedException("$key is not set in test environment")

    override fun token() = testAccessToken

    override fun anySavedShippingCost(): Pair<ShippingCost, PriceListChannelSearch> =
        anyShippingCost().let {
            val saved = saveShippingCost(it)
            saved to PriceListChannelSearch(
                priceListIdentifier = saved.priceListIdentifier,
                channelIdentifier = saved.channelIdentifier
            )
        }

    override fun anySavedShippingCosts(): ShippingCosts =
        anyShippingCosts().also {
            it.elements.map { p ->
                sut.save(p, testAccessToken).checkedBlock()
            }
        }

    override fun notFoundShippingCost() = PriceListChannelSearch(
        priceListIdentifier = newString(),
        channelIdentifier = newString()
    )

    private fun saveShippingCost(shippingCost: ShippingCost) =
        sut.save(shippingCost, testAccessToken)
            .checkedBlock()

    private fun initialize(accessToken: String): HttpShippingCostsSdk =
        HttpShippingCostsSdk.Builder(webClient)
            .path(samLocalPath)
            .build()
            .also {
                testAccessToken = accessToken
                println("starting local execution with ${testAccessToken.obfuscate()}")
            }

    private fun getAccessToken() = System.getenv(key)

    private fun String.obfuscate() = "${this.take(4)}..${this.takeLast(4)}"
}

class InMemoryShippingCostsSdkTest {

    companion object {
        private val firstCost = anyShippingCost()
        private val lastCost = anyShippingCost()
        private val costs = setOf(firstCost, lastCost)

        private val initializer = object : InMemoryShippingCostInitializer {

            private var costs = setOf<ShippingCost>()

            override fun setShippingCosts(shippingCosts: Set<ShippingCost>) {
                costs = shippingCosts.toMutableSet()
            }

            override fun getShippingCosts(): Set<ShippingCost> = costs
        }
    }

    private lateinit var sut: InMemoryShippingCostsSdk

    @Before
    fun setup() {
        sut = initializer.apply {
            setShippingCosts(costs)
        }.let {
            InMemoryShippingCostsSdk(it)
        }
    }

    @After
    fun after() {
        sut.deleteAll()
    }

    @Test
    fun `can retrieve shipping cost`() {
        val expected = firstCost

        StepVerifier.create(
            sut.findBy(PriceListChannelSearch(expected.priceListIdentifier, expected.channelIdentifier))
        ).assertNext {
            assertEquals(expected, it)
        }.verifyComplete()
    }

    @Test
    fun `can retrieve shipping costs`() {
        val expected = ShippingCosts(
            elements = listOf(firstCost, lastCost).sortedBy { it.channelIdentifier },
            scroll = null
        )

        StepVerifier.create(
            sut.findAll()
        ).assertNext {
            assertEquals(expected, it)
        }.verifyComplete()
    }

    @Test
    fun `fails for not found minimum purchase price`() {
        StepVerifier.create(
            sut.findBy(anySearch())
        ).expectError(NotFound::class.java).verify()
    }

    @Test
    fun `for found shipping cost ignores default value`() {
        val expected = firstCost
        val default = anyShippingCost()

        StepVerifier.create(
            sut.findByOrDefault(
                PriceListChannelSearch(expected.priceListIdentifier, expected.channelIdentifier),
                default
            )
        ).assertNext {
            assertEquals(expected, it)
        }.verifyComplete()
    }

    @Test
    fun `for not found shipping cost uses default value`() {
        val expected = anyShippingCost()

        StepVerifier.create(
            sut.findByOrDefault(anySearch(), expected)
        ).assertNext {
            assertEquals(expected, it)
        }.verifyComplete()
    }

    @Test
    fun `for found shipping cost does not throw given exception`() {
        val expected = firstCost
        val thrown = RuntimeException()

        StepVerifier.create(
            sut.findByOrThrow(PriceListChannelSearch(expected.priceListIdentifier, expected.channelIdentifier), thrown)
        ).assertNext {
            assertEquals(expected, it)
        }.verifyComplete()
    }

    @Test
    fun `for not found shipping cost throw given exception`() {
        val thrown = RuntimeException()

        StepVerifier.create(
            sut.findByOrThrow(anySearch(), thrown)
        ).expectError(thrown.javaClass).verify()
    }

    private fun anySearch() = PriceListChannelSearch(newString(), newString())
}
