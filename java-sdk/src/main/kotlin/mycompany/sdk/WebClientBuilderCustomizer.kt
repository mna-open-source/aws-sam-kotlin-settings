package mycompany.sdk

import org.springframework.web.reactive.function.client.*
import org.springframework.web.util.DefaultUriBuilderFactory
import reactor.core.publisher.Mono
import java.net.URI
import java.util.*


object WebClientBuilderCustomizer {
    fun customize(builder: WebClient.Builder, root: URI): WebClient.Builder {
        val factory = DefaultUriBuilderFactory(root.toString())
        factory.encodingMode = DefaultUriBuilderFactory.EncodingMode.VALUES_ONLY
        builder.uriBuilderFactory(factory)
        return builder
            .clone()
            .baseUrl(Objects.requireNonNull(root.toString()))
            .filter(ForbiddenHostsFilter(root.host))
    }

    internal class ForbiddenHostsFilter(private val allowedHost: String) : ExchangeFilterFunction {
        override fun filter(request: ClientRequest, next: ExchangeFunction): Mono<ClientResponse> {
            val uri = request.url()
            if (uri.host == null || !uri.host.equals(allowedHost, ignoreCase = true)) throw ForbiddenHost(uri)
            return next.exchange(request)
        }
    }
}
