package mycompany.sdk.common.setting.price

import mycompany.sdk.common.setting.*
import java.math.BigDecimal

data class MinimumPurchasePrice(
    val priceListIdentifier: String,
    val channelIdentifier: String,
    val price: BigDecimal
)

data class MinimumPurchasePrices(
    val prices: List<MinimumPurchasePrice>,
    val scroll: String?
)

fun MinimumPurchasePrice.asCommonSetting() =
    CommonSetting(
        identifier = Identifier.MINIMUM_PURCHASE_PRICE,
        namespace = namespace(priceListIdentifier, channelIdentifier),
        value = price.toString()
    )

fun PriceListChannelSearch.toFindSetting() =
    SearchObject(
        identifier = Identifier.MINIMUM_PURCHASE_PRICE,
        namespace = namespace(priceListIdentifier, channelIdentifier)
    )

fun CommonSetting.asMinimumPurchasePrice(): MinimumPurchasePrice = NamespaceHelper.from(namespace).let {
    MinimumPurchasePrice(
        priceListIdentifier = it.first,
        channelIdentifier = it.second,
        price = value.toBigDecimal()
    )
}

fun CommonSettings.asMinimumPurchasePrices() = MinimumPurchasePrices(
    prices = this.settings.map { it.asMinimumPurchasePrice() },
    scroll = scroll
)

private fun namespace(priceListIdentifier: String, channelIdentifier: String) =
    NamespaceHelper.from(
        NamespaceItem(Key.PRICE_LIST_ID, priceListIdentifier),
        NamespaceItem(Key.CHANNEL_ID, channelIdentifier)
    )
