package mycompany.sdk.common.setting.price

import org.slf4j.LoggerFactory
import org.springframework.web.reactive.function.client.WebClient

import mycompany.sdk.NotFound
import mycompany.sdk.common.setting.*
import mycompany.sdk.common.setting.PriceListChannelSearch
import reactor.core.publisher.Mono
import java.math.BigDecimal

interface MinimumPurchasePricesSdk {
    fun findAll(scroll: String? = null): Mono<MinimumPurchasePrices>
    fun findBy(search: PriceListChannelSearch): Mono<BigDecimal>
    fun save(minimumPurchasePrice: MinimumPurchasePrice, accessToken: String): Mono<MinimumPurchasePrice>
}

class HttpMinimumPurchasePricesSdk private constructor(
        pathWrapper: PathSdkBuilderWrapper? = null,
        nameSpaceWrapper: NamespaceSdkBuilderWrapper? = null,
        cacheConfiguration: CacheConfiguration,
        builder: WebClient.Builder
): HttpCommonSettingsSdk (
        namespace = nameSpaceWrapper?.namespace, path = pathWrapper?.path, builder = builder, cacheConfiguration = cacheConfiguration
), MinimumPurchasePricesSdk {

    override fun findAll(scroll: String?): Mono<MinimumPurchasePrices> =
            findSettingsByIdentifier(Identifier.MINIMUM_PURCHASE_PRICE, scroll).map {
                it.asMinimumPurchasePrices()
            }

    override fun findBy(search: PriceListChannelSearch): Mono<BigDecimal> =
            findSettingFor(search.toFindSetting()).map {
                it.value.toBigDecimal()
            }

    override fun save(minimumPurchasePrice: MinimumPurchasePrice, accessToken: String): Mono<MinimumPurchasePrice> =
            saveSetting(minimumPurchasePrice.asCommonSetting(), accessToken)
                    .map {
                        minimumPurchasePrice.copy(price = BigDecimal(it.value))
                    }

    class Builder(
            private val webBuilder: WebClient.Builder
    ): SdkBuilder<HttpMinimumPurchasePricesSdk>(builder = webBuilder) {

        override fun build() =
                wrapper().let {
                    when(it) {
                        is PathSdkBuilderWrapper -> HttpMinimumPurchasePricesSdk(pathWrapper = it, builder = webBuilder, cacheConfiguration = it.cacheConfiguration)
                        is NamespaceSdkBuilderWrapper -> HttpMinimumPurchasePricesSdk(nameSpaceWrapper = it, builder = webBuilder, cacheConfiguration = it.cacheConfiguration)
                        else -> throw CannotInitializeClient("Must provide service with name space or full path")
                    }
                }
    }
}

@Suppress("unused")
class InMemoryMinimumPurchasePricesSdk: MinimumPurchasePricesSdk {

    companion object {
        private val log = LoggerFactory.getLogger(InMemoryMinimumPurchasePricesSdk::class.java)
        private val prices = mutableMapOf<String, MinimumPurchasePrice>()
    }

    override fun findAll(scroll: String?): Mono<MinimumPurchasePrices> {
        log.warn("calling in memory findAll method")
        return prices.values.toList().let {
            Mono.just(
                    MinimumPurchasePrices(
                            it, null
                    )
            )
        }
    }

    override fun findBy(search: PriceListChannelSearch): Mono<BigDecimal> {
        log.warn("calling in memory findBy method")
        return Mono.fromSupplier {
            prices[search.asKey()].let {
                it?.price ?: throw NotFound()
            }
        }
    }

    override fun save(minimumPurchasePrice: MinimumPurchasePrice, accessToken: String): Mono<MinimumPurchasePrice> {
        log.warn("calling in memory clearAll method")
        return prices.put(minimumPurchasePrice.asKey(), minimumPurchasePrice).let {
            Mono.just(minimumPurchasePrice)
        }
    }

    fun deleteAll() {
        log.warn("calling in memory clearAll method")
        prices.clear()
    }

    private fun PriceListChannelSearch.asKey() = "${priceListIdentifier}!!${channelIdentifier}"
    private fun MinimumPurchasePrice.asKey() = "${priceListIdentifier}!!${channelIdentifier}"
}
