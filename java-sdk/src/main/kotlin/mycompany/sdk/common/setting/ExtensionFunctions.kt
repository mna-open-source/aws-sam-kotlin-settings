package mycompany.sdk.common.setting

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.json.JsonMapper
import com.fasterxml.jackson.module.kotlin.convertValue
import org.springframework.http.HttpStatus
import org.springframework.web.reactive.function.client.WebClient

import mycompany.sdk.HttpErrorHandler
import mycompany.sdk.WebClientBuilderCustomizer
import mycompany.sdk.common.setting.Mapper.mapper
import reactor.core.publisher.Mono
import java.net.URI

object Mapper {
    private lateinit var cachedMapper: ObjectMapper
    val mapper = if(::cachedMapper.isInitialized) {
        cachedMapper
    } else {
        JsonMapper().also {
            cachedMapper = it
        }
    }
}

object NamespaceHelper {
    private const val namespaceSeparator = ","
    private const val namespaceItemSeparator = "!!"

    fun from(vararg map: NamespaceItem): String =
            map.joinToString(namespaceSeparator) { "${it.key.name}$namespaceItemSeparator${it.value}" }

    fun from(namespace: String): Pair<String, String> =
            namespace.split(namespaceSeparator).let {
                it.first().split(namespaceItemSeparator).last() to it.last().split(namespaceItemSeparator).last()
            }
}

data class NamespaceItem(
        val key: Key,
        val value: String
)

fun JsonNode.toCommonSetting(): CommonSetting = mapper.convertValue(this)
fun JsonNode.toCommonSettings(): CommonSettings =
        mapper.convertValue(this)

fun String.toUri(): URI = URI.create(this)

fun WebClient.Builder.customize(root: String): WebClient =
        WebClientBuilderCustomizer.customize(this, root.toUri()).build()

fun WebClient.ResponseSpec.asCommonSetting(): Mono<CommonSetting> = this
        .onStatus(HttpStatus::isError, HttpErrorHandler::defaultHandling)
        .bodyToMono(JsonNode::class.java)
        .map { it.toCommonSetting() }
fun WebClient.ResponseSpec.asCommonSettings(): Mono<CommonSettings> = this
        .onStatus(HttpStatus::isError, HttpErrorHandler::defaultHandling)
        .bodyToMono(JsonNode::class.java)
        .map { it.toCommonSettings() }

