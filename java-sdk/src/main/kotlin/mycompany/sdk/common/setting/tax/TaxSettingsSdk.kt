package mycompany.sdk.common.setting.tax

import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Mono
import mycompany.sdk.NotFound
import mycompany.sdk.common.setting.*

interface TaxSettingsSdk {
    fun findAll(scroll: String? = null): Mono<TaxSettings>
    fun isTaxIncludedOn(priceListIdentifier: String): Mono<Boolean>
    fun save(taxSetting: TaxSetting, accessToken: String): Mono<TaxSetting>
}

class HttpTaxSettingsSdk private constructor(
        pathWrapper: PathSdkBuilderWrapper? = null,
        nameSpaceWrapper: NamespaceSdkBuilderWrapper? = null,
        cacheConfiguration: CacheConfiguration,
        builder: WebClient.Builder
): HttpCommonSettingsSdk(
        namespace = nameSpaceWrapper?.namespace, path = pathWrapper?.path, builder = builder, cacheConfiguration = cacheConfiguration
), TaxSettingsSdk {

    override fun findAll(scroll: String?): Mono<TaxSettings> =
            findSettingsByIdentifier(Identifier.TAX_SETTING, scroll).map {
                it.asTaxSettings()
            }

    override fun isTaxIncludedOn(priceListIdentifier: String): Mono<Boolean> =
            findSettingFor(priceListIdentifier.toFindTaxSetting())
                    .onErrorResume (NotFound::class.java){
                        fallBackValue(priceListIdentifier)
                    }.map {
                        it.asTaxSetting().isTaxIncluded
                    }

    override fun save(taxSetting: TaxSetting, accessToken: String): Mono<TaxSetting> =
            saveSetting(taxSetting.asCommonSetting(), accessToken).map {
                it.asTaxSetting()
            }

    private fun fallBackValue(priceListIdentifier: String) = Mono.just(CommonSetting(Identifier.TAX_SETTING, priceListIdentifier, "false"))

    class Builder(
            private val webBuilder: WebClient.Builder
    ): SdkBuilder<HttpTaxSettingsSdk>(builder = webBuilder) {

        override fun build() =
                wrapper().let {
                    when(it) {
                        is PathSdkBuilderWrapper -> HttpTaxSettingsSdk(pathWrapper = it, builder = webBuilder, cacheConfiguration = it.cacheConfiguration)
                        is NamespaceSdkBuilderWrapper -> HttpTaxSettingsSdk(nameSpaceWrapper = it, builder = webBuilder, cacheConfiguration = it.cacheConfiguration)
                        else -> throw CannotInitializeClient("Must provide service with name space or full path")
                    }
                }
    }
}
