package mycompany.sdk.common.setting.tax

import mycompany.sdk.common.setting.*

private const val namespaceItemSeparator = "!!"

data class TaxSetting(
    val priceListIdentifier: String,
    val isTaxIncluded: Boolean
)

data class TaxSettings(
    val settings: List<TaxSetting>,
    val scroll: String?
)

fun TaxSetting.asCommonSetting() = CommonSetting(
    identifier = Identifier.TAX_SETTING,
    namespace = "${Key.PRICE_LIST_ID}$namespaceItemSeparator$priceListIdentifier",
    value = isTaxIncluded.toString()
)

fun CommonSetting.asTaxSetting() = TaxSetting(
    priceListIdentifier = namespace.split(namespaceItemSeparator).last(),
    isTaxIncluded = value.toBoolean()
)

fun CommonSettings.asTaxSettings() = TaxSettings(
    settings = this.settings.map { it.asTaxSetting() },
    scroll = scroll
)

fun String.toFindTaxSetting() = SearchObject(
    identifier = Identifier.TAX_SETTING,
    namespace = "${Key.PRICE_LIST_ID}$namespaceItemSeparator$this"
)
