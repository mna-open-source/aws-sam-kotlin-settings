package mycompany.sdk.common.setting.shipping.cost

import org.slf4j.LoggerFactory
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Mono
import mycompany.sdk.NotFound
import mycompany.sdk.common.setting.*

interface ShippingCostsSdk {
    fun findAll(scroll: String? = null): Mono<ShippingCosts>
    fun findBy(search: PriceListChannelSearch): Mono<ShippingCost>
    fun findByOrDefault(search: PriceListChannelSearch, default: ShippingCost): Mono<ShippingCost>
    fun findByOrThrow(search: PriceListChannelSearch, thrown: Throwable): Mono<ShippingCost>
    fun save(shippingCost: ShippingCost, accessToken: String): Mono<ShippingCost>
}

class HttpShippingCostsSdk private constructor(
    pathWrapper: PathSdkBuilderWrapper? = null,
    nameSpaceWrapper: NamespaceSdkBuilderWrapper? = null,
    cacheConfiguration: CacheConfiguration,
    builder: WebClient.Builder
) : HttpCommonSettingsSdk(
    namespace = nameSpaceWrapper?.namespace,
    path = pathWrapper?.path,
    builder = builder,
    cacheConfiguration = cacheConfiguration
), ShippingCostsSdk {

    override fun findAll(scroll: String?): Mono<ShippingCosts> =
        findSettingsByIdentifier(Identifier.SHIPPING_COST, scroll).map {
            it.toShippingCost()
        }

    override fun findBy(search: PriceListChannelSearch): Mono<ShippingCost> =
        findSettingFor(search.toFindSetting())
            .map {
                it.toShippingCost()
            }

    override fun findByOrDefault(search: PriceListChannelSearch, default: ShippingCost): Mono<ShippingCost> =
        findSettingForOrDefault(search.toFindSetting(), default.toCommonSetting())
            .toShippingCost()

    override fun findByOrThrow(search: PriceListChannelSearch, thrown: Throwable): Mono<ShippingCost> =
        findSettingForOrThrow(search.toFindSetting(), thrown)
            .toShippingCost()

    override fun save(shippingCost: ShippingCost, accessToken: String): Mono<ShippingCost> =
        saveSetting(shippingCost.toCommonSetting(), accessToken)
            .toShippingCost()

    class Builder(
        private val webBuilder: WebClient.Builder
    ) : SdkBuilder<HttpShippingCostsSdk>(builder = webBuilder) {

        override fun build() =
            wrapper().let {
                when (it) {
                    is PathSdkBuilderWrapper -> HttpShippingCostsSdk(
                        pathWrapper = it,
                        builder = webBuilder,
                        cacheConfiguration = it.cacheConfiguration
                    )
                    is NamespaceSdkBuilderWrapper -> HttpShippingCostsSdk(
                        nameSpaceWrapper = it,
                        builder = webBuilder,
                        cacheConfiguration = it.cacheConfiguration
                    )
                    else -> throw CannotInitializeClient("Must provide service with name space or full path")
                }
            }
    }

    private fun Mono<CommonSetting>.toShippingCost() = this.map {
        it.toShippingCost()
    }
}

@Suppress("unused")
class InMemoryShippingCostsSdk(
    initializer: InMemoryShippingCostInitializer? = null
) : ShippingCostsSdk {

    companion object {
        private val log = LoggerFactory.getLogger(InMemoryShippingCostsSdk::class.java)
        private val shippingCosts = mutableMapOf<String, ShippingCost>()
    }

    init {
        initializer?.getShippingCosts()?.forEach {
            shippingCosts[it.asKey()] = it
        }
    }

    override fun findAll(scroll: String?): Mono<ShippingCosts> {
        log.warn("calling in memory findAll method")
        return shippingCosts.values.toList().sortedBy { it.channelIdentifier }.let {
            Mono.just(
                ShippingCosts(
                    elements = it,
                    scroll = null
                )
            )
        }
    }

    override fun findBy(search: PriceListChannelSearch): Mono<ShippingCost> {
        log.warn("calling in memory findBy method")
        return shippingCosts[search.asKey()].let {
            if (it != null) Mono.just(it)
            else Mono.error(NotFound())
        }
    }

    override fun findByOrDefault(search: PriceListChannelSearch, default: ShippingCost): Mono<ShippingCost> {
        log.warn("calling in memory findByOrDefault method")
        return shippingCosts[search.asKey()].let {
            if (it != null) Mono.just(it)
            else Mono.just(default)
        }
    }

    override fun findByOrThrow(search: PriceListChannelSearch, thrown: Throwable): Mono<ShippingCost> {
        log.warn("calling in memory findByOrDefault method")
        return shippingCosts[search.asKey()].let {
            if (it != null) Mono.just(it)
            else Mono.error(thrown)
        }
    }

    override fun save(shippingCost: ShippingCost, accessToken: String): Mono<ShippingCost> {
        log.warn("calling in memory save method")
        return shippingCosts.put(shippingCost.asKey(), shippingCost).let {
            Mono.just(shippingCost)
        }
    }

    fun deleteAll() {
        log.warn("calling in memory clearAll method")
        shippingCosts.clear()
    }

    private fun PriceListChannelSearch.asKey() = "${priceListIdentifier}!!${channelIdentifier}"
    private fun ShippingCost.asKey() = "${priceListIdentifier}!!${channelIdentifier}"
}

@Suppress("unused")
interface InMemoryShippingCostInitializer {
    fun setShippingCosts(shippingCosts: Set<ShippingCost>)
    fun getShippingCosts(): Set<ShippingCost>
}
