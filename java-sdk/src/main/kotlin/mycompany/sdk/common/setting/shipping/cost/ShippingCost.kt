package mycompany.sdk.common.setting.shipping.cost

import com.fasterxml.jackson.module.kotlin.readValue
import mycompany.sdk.common.setting.*
import java.math.BigDecimal

data class Money(
    val amount: BigDecimal,
    val currency: String
) {
    constructor() : this(
        amount = BigDecimal.ZERO,
        currency = "USD",
    )

    init {
        require(amount >= BigDecimal.ZERO) {
            IllegalArgumentException("amount cannot be smaller than zero")
        }
    }
}

data class ShippingCost(
    val priceListIdentifier: String,
    val channelIdentifier: String,
    val value: Money
)

data class ShippingCosts(
    val elements: List<ShippingCost>,
    val scroll: String?
)

fun PriceListChannelSearch.toFindSetting() = SearchObject(
    identifier = Identifier.SHIPPING_COST,
    namespace = namespace(priceListIdentifier, channelIdentifier)
)

fun CommonSetting.toShippingCost() = NamespaceHelper.from(namespace).let {
    ShippingCost(
        priceListIdentifier = it.first,
        channelIdentifier = it.second,
        value = value.asMoney()
    )
}

fun CommonSettings.toShippingCost() = ShippingCosts(
    elements = this.settings.map { it.toShippingCost() },
    scroll = this.scroll
)

fun ShippingCost.toCommonSetting() = CommonSetting(
    identifier = Identifier.SHIPPING_COST,
    namespace = namespace(priceListIdentifier, channelIdentifier),
    value = Mapper.mapper.writeValueAsString(value)
)

fun String.asMoney(): Money = Mapper.mapper.readValue(this)

private fun namespace(priceListIdentifier: String, channelIdentifier: String) =
    NamespaceHelper.from(
        NamespaceItem(Key.PRICE_LIST_ID, priceListIdentifier),
        NamespaceItem(Key.CHANNEL_ID, channelIdentifier)
    )
