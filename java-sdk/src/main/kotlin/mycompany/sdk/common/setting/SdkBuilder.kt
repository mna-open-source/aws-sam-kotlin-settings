package mycompany.sdk.common.setting


import com.github.benmanes.caffeine.cache.Cache
import com.github.benmanes.caffeine.cache.Caffeine
import org.slf4j.LoggerFactory
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.BodyInserters
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Mono
import software.amazon.awssdk.services.servicediscovery.ServiceDiscoveryAsyncClient
import software.amazon.awssdk.services.servicediscovery.model.DiscoverInstancesRequest
import mycompany.sdk.NotFound
import java.util.concurrent.TimeUnit

interface CommonSettingsSdk {
    fun findSettingsByIdentifier(identifier: Identifier, scroll: String?): Mono<CommonSettings>
    fun findSettingsByIdentifier(identifier: String, scroll: String?): Mono<CommonSettings>
    fun findSettingFor(searchObject: SearchObject): Mono<CommonSetting>
    fun findSettingForOrDefault(searchObject: SearchObject, default: CommonSetting): Mono<CommonSetting>
    fun findSettingForOrThrow(searchObject: SearchObject, thrown: Throwable): Mono<CommonSetting>
    fun saveSetting(commonSetting: CommonSetting, accessToken: String): Mono<CommonSetting>
}

open class HttpCommonSettingsSdk(
    path: String? = null,
    val namespace: String? = null,
    val builder: WebClient.Builder,
    val serviceName: String = "CommonSettingsApiFunction",
    cacheConfiguration: CacheConfiguration
) : CommonSettingsSdk {

    companion object {
        private const val INSTANCE_CNAME = "AWS_INSTANCE_CNAME"
        private const val ROOT_PATH = "common-setting"
        private const val FIND_BY_IDENTIFIER = "common-settings-by-identifier"

        private const val identifierKey = "identifier"
        private const val namespaceKey = "namespace"
        private const val scrollKey = "scroll"

        private val log = LoggerFactory.getLogger(HttpCommonSettingsSdk::class.java)
    }

    private var http: WebClient
    private var cache: Cache<String, Cacheable>

    private lateinit var root: String

    init {
        http = when {
            namespace != null -> withNamespace()
            path != null -> webClient(path)
            else -> throw CannotInitializeClient("must provide a service name or path")
        }
        cache = Caffeine.newBuilder()
            .expireAfterWrite(cacheConfiguration.expiresAfter, cacheConfiguration.expiresAfterUnit)
            .maximumSize(cacheConfiguration.size)
            .recordStats()
            .build()
    }

    override fun findSettingsByIdentifier(identifier: Identifier, scroll: String?): Mono<CommonSettings> =
        get(identifier, scroll, ::findByIdentifier)

    override fun findSettingsByIdentifier(identifier: String, scroll: String?): Mono<CommonSettings> =
        get(identifier, scroll, ::findByIdentifier)

    override fun findSettingFor(searchObject: SearchObject): Mono<CommonSetting> =
        get(searchObject, ::findBySearch)

    override fun findSettingForOrDefault(searchObject: SearchObject, default: CommonSetting): Mono<CommonSetting> =
        get(searchObject, ::findBySearch)
            .onErrorResume(NotFound::class.java) {
                Mono.just(default)
            }

    override fun findSettingForOrThrow(searchObject: SearchObject, thrown: Throwable): Mono<CommonSetting> =
        get(searchObject, ::findBySearch)
            .onErrorMap(NotFound::class.java) {
                thrown
            }

    override fun saveSetting(commonSetting: CommonSetting, accessToken: String): Mono<CommonSetting> = http
        .post()
        .uri(ROOT_PATH)
        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
        .header(HttpHeaders.AUTHORIZATION, accessToken.asBearerToken())
        .body(BodyInserters.fromValue(commonSetting))
        .retrieve()
        .asCommonSetting()
        .doOnSuccess {
            invalidate(it)
        }

    private fun findByIdentifier(identifier: Identifier, scroll: String?): Mono<CommonSettings> =
        findByIdentifier(identifier.name, scroll)

    private fun findByIdentifier(identifier: String, scroll: String?): Mono<CommonSettings> = http
        .get()
        .uri {
            it.path("{path}").apply {
                queryParam(identifierKey, identifier)
                if (scroll != null) {
                    queryParam(scrollKey, scroll)
                }
            }.build(FIND_BY_IDENTIFIER)
        }
        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
        .retrieve()
        .asCommonSettings()

    private fun findBySearch(searchObject: SearchObject): Mono<CommonSetting> = http
        .get()
        .uri {
            it.path("{path}").apply {
                queryParam(namespaceKey, searchObject.namespace)
                queryParam(identifierKey, searchObject.identifier)
            }.build(ROOT_PATH)
        }
        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
        .retrieve()
        .asCommonSetting()

    private fun get(
        identifier: Identifier,
        scroll: String?,
        block: (Identifier, String?) -> Mono<CommonSettings>
    ): Mono<CommonSettings> =
        cache.getIfPresent("${identifier.name}$scroll")
            .takeIf { it != null }?.let {
                Mono.just(it as CommonSettings)
            } ?: invoke(identifier, scroll, block)

    private fun get(
        identifier: String,
        scroll: String?,
        block: (String, String?) -> Mono<CommonSettings>
    ): Mono<CommonSettings> =
        cache.getIfPresent("${identifier}$scroll")
            .takeIf { it != null }?.let {
                Mono.just(it as CommonSettings)
            } ?: invoke(identifier, scroll, block)

    private fun get(searchObject: SearchObject, block: (SearchObject) -> Mono<CommonSetting>): Mono<CommonSetting> =
        cache.getIfPresent(searchObject.key)
            .takeIf { it != null }?.let {
                Mono.just(it as CommonSetting)
            } ?: invoke(searchObject, block)

    private fun invalidate(commonSetting: CommonSetting) {
        log.info("about to invalidate cache for {}", commonSetting)
        cache.invalidateAll()
    }

    private fun invoke(searchObject: SearchObject, block: (SearchObject) -> Mono<CommonSetting>) =
        log.debug("about to invoke {} for {}", block, searchObject).let {
            block(searchObject).doOnSuccess {
                cache.put(searchObject.key, it)
            }
        }

    private fun invoke(identifier: Identifier, scroll: String?, block: (Identifier, String?) -> Mono<CommonSettings>) =
        log.debug("about to invoke {} for scroll {}", block, scroll).let {
            block(identifier, scroll).doOnSuccess {
                cache.put("${Identifier.MINIMUM_PURCHASE_PRICE}$scroll", it)
            }
        }

    private fun invoke(identifier: String, scroll: String?, block: (String, String?) -> Mono<CommonSettings>) =
        log.debug("about to invoke {} for scroll {}", block, scroll).let {
            block(identifier, scroll).doOnSuccess {
                cache.put("${Identifier.MINIMUM_PURCHASE_PRICE}$scroll", it)
            }
        }

    private fun webClient(path: String) =
        builder.customize(path).also {
            root = path
        }

    private fun withNamespace() =
        ServiceDiscoveryAsyncClient
            .builder()
            .build()
            .discoverInstances(
                DiscoverInstancesRequest.builder()
                    .namespaceName(namespace)
                    .serviceName(serviceName)
                    .build()
            ).get().let {
                it.instances().firstOrNull()?.attributes()!![INSTANCE_CNAME]
                    ?: throw ServiceInstanceNotFound(serviceName, namespace!!)
            }.let {
                webClient(it)
            }

    private fun String.asBearerToken() = "Bearer $this"
}

abstract class SdkBuilder<T : HttpCommonSettingsSdk>(
    private var builder: WebClient.Builder
) {

    private lateinit var url: String
    private lateinit var namespaceString: String
    private lateinit var cacheConfig: CacheConfiguration

    /**
     * Convenience method to help to configure cache used on this sdk.
     */
    fun cacheConfiguration(cacheConfiguration: CacheConfiguration) = apply { cacheConfig = cacheConfiguration }

    fun namespace(namespace: String) = apply { this.namespaceString = namespace }

    /**
     * Provides the path to where this service can be invoked, has priority over {@link #namespace(String) namespace} method
     */
    fun path(path: String) = apply { this.url = path }

    /**
     * Build this sdk with configuration provided, by default, it configures a cache with default values.
     * The cache default values can be found on { @link mycompany.sdk.common.setting.CacheConfiguration CacheConfiguration }
     */
    abstract fun build(): T

    fun wrapper(): SdkBuilderWrapper {
        val cacheConfiguration = if (::cacheConfig.isInitialized) cacheConfig else CacheConfiguration()
        return when {
            ::url.isInitialized -> PathSdkBuilderWrapper(url, cacheConfiguration)
            ::namespaceString.isInitialized -> NamespaceSdkBuilderWrapper(namespaceString, cacheConfiguration)
            else -> throw CannotInitializeClient("Must provide service with name space or full path")
        }
    }
}

interface SdkBuilderWrapper

data class PathSdkBuilderWrapper(
    val path: String,
    val cacheConfiguration: CacheConfiguration
) : SdkBuilderWrapper

data class NamespaceSdkBuilderWrapper(
    val namespace: String,
    val cacheConfiguration: CacheConfiguration
) : SdkBuilderWrapper

data class CacheConfiguration(
    val expiresAfter: Long = 5,
    val expiresAfterUnit: TimeUnit = TimeUnit.MINUTES,
    val size: Long = 500
)

data class CannotInitializeClient(val reason: String) : RuntimeException(reason)
data class ServiceInstanceNotFound(val service: String, val namespace: String) :
    RuntimeException("Service $service cannot be found on $namespace")



