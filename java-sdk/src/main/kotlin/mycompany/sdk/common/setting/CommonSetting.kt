package mycompany.sdk.common.setting

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

interface Cacheable

@JsonIgnoreProperties(ignoreUnknown = true)
data class CommonSetting(
    val identifier: String,
    val namespace: String,
    val value: String
) : Cacheable {
    constructor(): this(
        identifier = Identifier.NONE,
        namespace = "",
        value = ""
    )

    constructor(
        identifier: Identifier,
        namespace: String,
        value: String
    ) : this(
        identifier = identifier.name,
        namespace = namespace,
        value = value
    )
}

@JsonIgnoreProperties(ignoreUnknown = true)
data class CommonSettings(
    val identifier: String,
    val settings: List<CommonSetting>,
    val scroll: String?
) : Cacheable {
    constructor(): this(
        identifier = Identifier.NONE,
        settings = listOf(),
        scroll = null
    )

    constructor(
        identifier: Identifier,
        settings: List<CommonSetting>,
        scroll: String?
    ) : this(
        identifier = identifier.name,
        settings = settings,
        scroll = scroll
    )
}

data class PriceListChannelSearch(
    val priceListIdentifier: String,
    val channelIdentifier: String
)

data class SearchObject(
    val identifier: Identifier,
    val namespace: String
) {
    val key = "$identifier!!$namespace"
}

enum class Identifier {
    MINIMUM_PURCHASE_PRICE,
    SHIPPING_COST,
    TAX_SETTING,
    NONE
}

enum class Key {
    PRICE_LIST_ID,
    CHANNEL_ID;
}
