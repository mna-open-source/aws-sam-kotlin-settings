package mycompany.sdk

import com.fasterxml.jackson.databind.JsonNode
import org.slf4j.Logger
import org.springframework.http.HttpStatus
import org.springframework.web.reactive.function.client.ClientResponse
import reactor.core.publisher.Mono
import java.util.function.Function

object HttpErrorHandler {

    fun defaultHandling(clientResponse: ClientResponse?): Mono<out Throwable?>? {
        return defaultHandling(
            clientResponse!!
        ) { error: GenericClientError? -> error }
    }

    fun defaultHandling(
        clientResponse: ClientResponse,
        mapper: Function<GenericClientError?, Throwable?>
    ): Mono<out Throwable?>? {
        return when (clientResponse.statusCode()) {
            HttpStatus.UNAUTHORIZED -> Mono.error(AccessDeniedException())
            HttpStatus.FORBIDDEN -> Mono.error(ForbiddenOperationException(clientResponse.statusCode().reasonPhrase))
            HttpStatus.BAD_REQUEST, HttpStatus.CONFLICT -> defaultBadRequestAndConflictHandling(clientResponse)
                .map {
                    var t = unwrapConcurrentException(it)
                    t = if (t === it) unwrapMoneyRequestTimeExceeded(it) else t
                    if (t === it) mapper.apply(it) else t
                }
            HttpStatus.NOT_FOUND -> Mono.error(NotFound())
            else -> Mono.error(UnexpectedResponse(clientResponse.statusCode()))
        }
    }

    private fun unwrapConcurrentException(error: GenericClientError): Throwable {
        return if (error.propertyMatches("concurrent-account-modification")) ConcurrentAccountModification() else error
    }

    private fun unwrapMoneyRequestTimeExceeded(error: GenericClientError): Throwable {
        return if (error.entityMatches("MoneyRequest") && error.propertyMatches("expiryDate")) MoneyRequestTimeExceeded() else error
    }

    private fun defaultBadRequestAndConflictHandling(r: ClientResponse): Mono<GenericClientError> {
        return r
            .bodyToMono(JsonNode::class.java)
            .map { jsonNode: JsonNode ->
                if (jsonNode.isArray) jsonNode.elements() else listOf(
                    jsonNode
                ).iterator()
            }
            .map { obj: Iterator<JsonNode?> -> obj.next() }
            .map { error: JsonNode? ->
                parseGenericClientError(
                    error!!
                )
            }
            .defaultIfEmpty(GenericClientError("HttpErrorHandler", "", "Bad request or conflict returned empty body"))
    }

    private fun parseGenericClientError(error: JsonNode): GenericClientError {
        return GenericClientError(
            if (error.hasNonNull("entity")) error["entity"].asText() else null,
            if (error.hasNonNull("property")) error["property"].asText() else null,
            if (error.hasNonNull("message")) error["message"].asText() else null
        )
    }

    fun defaultRetryMatcher(throwable: Throwable?, logger: Logger): Boolean {
        val isConcurrencyError = throwable is ConcurrentAccountModification
        logger.trace("ConcurrentAccountModification: {}", isConcurrencyError)
        return isConcurrencyError
    }
}
