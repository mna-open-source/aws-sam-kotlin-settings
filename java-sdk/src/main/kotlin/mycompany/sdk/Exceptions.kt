package mycompany.sdk

import org.springframework.http.HttpStatus
import java.net.URI

class NotFound : RuntimeException()

class AccessDeniedException : RuntimeException()

class ForbiddenOperationException(message: String) : RuntimeException(message)

class UnexpectedResponse(status: HttpStatus) :
    RuntimeException("Response status from backend is $status")

class ConcurrentAccountModification : RuntimeException()

class MoneyRequestTimeExceeded : RuntimeException()

class ForbiddenHost(uri: URI) : RuntimeException("forbidden host for URI: $uri")
