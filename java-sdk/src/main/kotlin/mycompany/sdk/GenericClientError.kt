package mycompany.sdk

import java.util.*


class GenericClientError(private val entity: String?, private val property: String?, private val reason: String?) :
    RuntimeException() {

    fun propertyMatches(s: String): Boolean {
        return s == property
    }

    fun entityMatches(s: String): Boolean {
        return s == entity
    }

    override val message: String
        get() = toString()

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val that = other as GenericClientError
        return entity == that.entity &&
                property == that.property &&
                reason == that.reason
    }

    override fun hashCode(): Int {
        return Objects.hash(entity, property, reason)
    }

    override fun toString(): String {
        return "GenericClientError{" +
                "entity='" + entity + '\'' +
                ", property='" + property + '\'' +
                ", message='" + reason + '\'' +
                '}'
    }
}
