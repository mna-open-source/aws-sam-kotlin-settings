plugins {
    kotlin("jvm")
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    withSourcesJar()
}

configurations.all {
    exclude(module = "slf4j-simple")
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:1.4.0")
    implementation("org.jetbrains.kotlin:kotlin-reflect:1.4.0")

    implementation(platform("io.projectreactor:reactor-bom:2020.0.10"))
    implementation("io.projectreactor:reactor-core:3.4.9")
    implementation("org.springframework.boot:spring-boot-starter-webflux:2.5.4")

    //jackson
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.12.5")
    implementation("com.fasterxml.jackson.datatype:jackson-datatype-jsr310:2.12.5")
    implementation("com.fasterxml.jackson.dataformat:jackson-dataformat-ion:2.12.5")
    implementation("com.fasterxml.jackson.dataformat:jackson-dataformat-cbor:2.12.5")

    //aws
    implementation(platform("software.amazon.awssdk:bom:2.14.23"))
    implementation("software.amazon.awssdk:servicediscovery:2.17.34")

    // WARNING: Do not update
    implementation("com.github.ben-manes.caffeine:caffeine:2.9.2")

    testImplementation("org.jetbrains.kotlin:kotlin-test-junit:1.5.21")
    testImplementation("com.nhaarman.mockitokotlin2:mockito-kotlin:2.2.0")
    testImplementation("io.projectreactor:reactor-test:3.4.9")
    testImplementation("junit:junit:4.13.2")
    testImplementation("org.slf4j:slf4j-simple:1.7.32")
    testImplementation("com.squareup.okhttp3:mockwebserver:4.9.1")
}

val compileKotlin: org.jetbrains.kotlin.gradle.tasks.KotlinCompile by tasks
compileKotlin.kotlinOptions {
    jvmTarget = "1.8"
}

val compileTestKotlin: org.jetbrains.kotlin.gradle.tasks.KotlinCompile by tasks
compileTestKotlin.kotlinOptions {
    jvmTarget = "1.8"
}

/*
publishing {
    publications {
        register("maven", MavenPublication::class.java) {
            from(components["java"])
            groupId = "mycompany.sdk"
            artifactId = "common-settings"
        }
    }
    repositories {
        maven {
            setUrl("s3://artifacts00000000000000000000000000.s3-us-east-1.amazonaws.com/maven2")
            authentication {
                create<AwsImAuthentication>("awsIm")
            }
        }
    }
}
 */

tasks.withType<JavaCompile> {
    options.encoding = "UTF-8"
}

tasks.test {
    reports {
        junitXml.isEnabled = true
    }
    testLogging {
        events = setOf(org.gradle.api.tasks.testing.logging.TestLogEvent.FAILED)
        showExceptions = true
        exceptionFormat = org.gradle.api.tasks.testing.logging.TestExceptionFormat.FULL
    }
}

group = "mycompany.sdk"
version = "0.0.1"

/*
sdkPublisher {
    artifact.id = "common-settings"
}
 */
